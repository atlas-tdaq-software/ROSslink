#ifndef _S5933_H_
#define _S5933_H_
/*---------------------------------------------------------------------------*/
/* s5933lib.h -- AMCC S5933 PCI Controller library                           */
/*                                                                           */
/* AMCC S5933 PCI Operation Registers definitions                            */
/*                                                                           */
/* D. Francis                                                                */
/* P. Kapinos  6/27/96                                                       */
/*                                                                           */
/* Note: Byte ordering takes into account that CPU is big endian & PCI is    */
/*       little endian.                                                      */
/*---------------------------------------------------------------------------*/

#include "rcc_error/rcc_error.h"

#define s5933_regs   unsigned int
#define s5933_mcsr   unsigned int
#define s5933_intcsr unsigned int

/*S5933 PCI bus Operation Registers*/
const unsigned int S5933_OMB1   = 0x00;  /* Outgoing Mailbox Registers       */
const unsigned int S5933_OMB2   = 0x04;  
const unsigned int S5933_OMB3   = 0x08;
const unsigned int S5933_OMB4   = 0x0c;

const unsigned int S5933_IMB1   = 0x10;  /* Incoming Mailbox Registers       */
const unsigned int S5933_IMB2   = 0x14;
const unsigned int S5933_IMB3   = 0x18;
const unsigned int S5933_IMB4   = 0x1c;

const unsigned int S5933_FIFO   = 0x20;  /* bidirectional FIFO Reg Port      */

const unsigned int S5933_MWAR   = 0x24;  /* Master Write Address Reg         */
const unsigned int S5933_MWTC   = 0x28;  /* Master Write Transfer Count Reg  */
const unsigned int S5933_MRAR   = 0x2c;  /* Master Read Address Reg          */
const unsigned int S5933_MRTC   = 0x30;  /* Master Read Transfer Count Reg   */
const unsigned int S5933_MBEF   = 0x34;  /* Mailbox Empty/Full Status Reg    */
const unsigned int S5933_INTCSR = 0x38;  /* Interrupt Control/Status Reg     */
const unsigned int S5933_MCSR   = 0x3c;  /* Bus Master Control/Status Reg    */

/*INTCSR fields*/
const unsigned int S5933_INTCSR_OUF     = 0x80000000;
const unsigned int S5933_INTCSR_INF     = 0x40000000;
const unsigned int S5933_INTCSR_FACA    = 0x30000000;
const unsigned int S5933_INTCSR_FACP    = 0x0c000000;
const unsigned int S5933_INTCSR_EC      = 0x03000000;
const unsigned int S5933_INTCSR_INTA    = 0x00800000;
const unsigned int S5933_INTCSR_TABORT  = 0x00200000;
const unsigned int S5933_INTCSR_MABORT  = 0x00100000;
const unsigned int S5933_INTCSR_RTC     = 0x00080000;
const unsigned int S5933_INTCSR_WTC     = 0x00040000;
const unsigned int S5933_INTCSR_IN      = 0x00020000;
const unsigned int S5933_INTCSR_OUT     = 0x00010000;
const unsigned int S5933_INTCSR_RTCIEN  = 0x00008000;
const unsigned int S5933_INTCSR_WTCIEN  = 0x00004000;
const unsigned int S5933_INTCSR_INIEN   = 0x00001000;
const unsigned int S5933_INTCSR_INMBOX  = 0x00000c00;
const unsigned int S5933_INTCSR_INBYTE  = 0x00000300;
const unsigned int S5933_INTCSR_OUTIEN  = 0x00000010;
const unsigned int S5933_INTCSR_OUTMBOX = 0x0000000c;
const unsigned int S5933_INTCSR_OUTBYTE = 0x00000003;

const unsigned int S5933_INTCSR_NOBITEC = 0x00000000;
const unsigned int S5933_INTCSR_16BITEC = 0x01000000;
const unsigned int S5933_INTCSR_32BITEC = 0x02000000;
const unsigned int S5933_INTCSR_64BITEC = 0x03000000;


/*MCRS fields*/

const unsigned int S5933_MCSR_NVCTL = 0xe0000000;

const unsigned int S5933_MCSR_MFR   = 0x08000000;
const unsigned int S5933_MCSR_A2PFR = 0x04000000;
const unsigned int S5933_MCSR_P2AFR = 0x02000000;
const unsigned int S5933_MCSR_AOR   = 0x01000000;
const unsigned int S5933_MCSR_NVOP  = 0x00ff0000;

const unsigned int S5933_MCSR_RTE   = 0x00004000;
const unsigned int S5933_MCSR_RMS   = 0x00002000;
const unsigned int S5933_MCSR_RVW   = 0x00001000;

const unsigned int S5933_MCSR_WTE   = 0x00000400;
const unsigned int S5933_MCSR_WMS   = 0x00000200;
const unsigned int S5933_MCSR_WVR   = 0x00000100;

const unsigned int S5933_MCSR_A2PTC = 0x00000080;
const unsigned int S5933_MCSR_P2ATC = 0x00000040;
const unsigned int S5933_MCSR_A2PFE = 0x00000020;
const unsigned int S5933_MCSR_A2PF4 = 0x00000010;
const unsigned int S5933_MCSR_A2PFF = 0x00000008;
const unsigned int S5933_MCSR_P2AFE = 0x00000004;
const unsigned int S5933_MCSR_P2AF4 = 0x00000002;
const unsigned int S5933_MCSR_P2AFF = 0x00000001;


enum s5933_returned_code {
  S5933_OK           = 0x00,
  S5933_ISOPEN       = (P_ID_S5933 << 8) + 0x01,
  S5933_NOTOPEN,
  S5933_MBOXINVALID,
  S5933_NOTDONE,
  S5933_SIZELIMIT,
  S5933_ADDBOUND,
  S5933_NOTMYFAULT,
  S5933_TIMEOUT,
  S5933_UNKNOWN,
  S5933_NOCODE
};


#ifdef __cplusplus
extern "C" {
#endif

/* function prototypes */
err_type S5933_Open                (s5933_regs *); 
err_type S5933_Close               (s5933_regs *);
err_type S5933_Reset               (s5933_regs *); 
err_type S5933_Dump                (s5933_regs *); 
err_type S5933_Write_Mailbox       (s5933_regs *, int mbox, int mbyte, unsigned int data);
err_type S5933_Read_Mailbox        (s5933_regs *, int mbox, int byte, unsigned int *data);
err_type S5933_Read_FIFO           (s5933_regs *, register unsigned int *dest, int *nwords);
err_type S5933_Write_FIFO          (s5933_regs *, register unsigned int *src, int *nwords);
err_type S5933_DMA_Read            (s5933_regs *, unsigned int addr, int size); 
err_type S5933_DMA_Write           (s5933_regs *, unsigned int addr, int size);
err_type S5933_DMA_Read_Buffer     (s5933_regs *, void *base, int size); 
err_type S5933_DMA_Write_Buffer    (s5933_regs *, void *base, int size);
err_type S5933_Wait_Endof_DMA_Read (s5933_regs *, int timeout);
err_type S5933_Wait_Endof_DMA_Write(s5933_regs *, int timeout);

#ifdef __cplusplus
}
#endif

#endif

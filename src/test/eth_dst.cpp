/*****************************************************************************/
/*                                                                           */
/* File Name        : eth_dst.c                                              */
/* Version          : 1.0                                                    */
/* Author           : Markus Joos (based on slink_dst_dvs.c by B. Gorini)    */
/*                                                                           */
/*****************************************************************************/
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <ctype.h>
// includes for TCP/IP
#include <sys/time.h>    // select struct timeval  
#include <string.h>      // memset & bzero in FD_ZERO  
#include <netinet/in.h>  // struct sockaddr_in ...  
#include <arpa/inet.h>   // for inet_addr  
#include <netinet/tcp.h> // TCP_NODELAY  
#include <sys/socket.h>  // socket setsockopt bind listen connect  
#include <netdb.h>       // getprotobyname gethostbyaddr  
#include <errno.h>       // errno  
#include "rcc_error/rcc_error.h"
#include "ROSslink/slink.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSEventFragment/ROBFragment.h"


using namespace ROS;

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif

// output codes (for dvs)
#define TEST_OK                 0
#define TEST_NOT_OK             183
#define TEST_UNRESOLVED         184

// constants 
#define MAX_DEVICES             0x7
#define MAX_PACKET_SIZE_WRDS    2048

// types 
typedef enum printType { SUMMARY = 0, FULL = 1 } PrintType;
typedef enum checkType { SIZE, L1ID, GLOBAL } CheckType;

// global variables 
int verbose = FALSE;
int print = FALSE;
int dcheck = FALSE;
int L1extdcheck = FALSE; 
int npackets = -1;
int occurence = 10000;
int ipacket;
int lpacket;
int delta_time;
int page_size;
int last_packet_size, packet_size;
int flg_error;
int fd;
int test_status = TEST_OK;
int writefile = FALSE;
int outputFile;
char filename[200] = {0};
int checkfile = FALSE;
int inputFile;
char filename2[200] = {0};
int errors[GLOBAL + 1] = { GLOBAL * 0, 0 };
int errorsTot[GLOBAL + 1] = { GLOBAL * 0, 0 };
unsigned int last_l1id = 999999;
unsigned int rec_size, exp_size;
float t_per_packet, packet_per_s, mb_per_s;
long start_time, end_time;
struct sockaddr_in sockin;
struct sockaddr_in sout;
socklen_t slen;
CheckType ccode;
char refbuf[0x100000];


/*********************************/
static int SetSocketOptions(int fd)
/*********************************/
{
  int sockopt = 0;
  int SOCKET_ERROR = -1;
  static const int c_so_rcvbuf = 256*1024; /* limit set by Linux! */
  static const int c_so_sndbuf = 256*1024; /* limit set by Linux! */

  // setsockopt SO_REUSEADDR - enable/disable local address reuse 
  sockopt = 1;
  if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char*)&sockopt, sizeof(sockopt)) == SOCKET_ERROR)
  {
    perror("setsockopt: ");
    return -1;
  }

  // setsockopt SO_SNDBUF - set buffer size for output 
  sockopt = c_so_sndbuf;
  if (setsockopt(fd, SOL_SOCKET, SO_SNDBUF, (char*)&sockopt, sizeof(sockopt)) == SOCKET_ERROR)
  {
    perror("setsockopt: ");
    return -1;
  }
 
  // setsockopt SO_RCVBUF - set buffer size for input 
  sockopt = c_so_rcvbuf;
  if (setsockopt(fd, SOL_SOCKET, SO_RCVBUF, (char*)&sockopt, sizeof(sockopt)) == SOCKET_ERROR)
  {
    perror("setsockopt: ");
    return -1;
  }

  // setsockopt TCP_NODELAY - to defeat the packetization algorithm 
  sockopt = 0; /* 0 to enable Nagle algorithm */
  if (setsockopt(fd, getprotobyname("tcp")->p_proto, TCP_NODELAY, (char *)&sockopt, sizeof(sockopt)) == SOCKET_ERROR)
  {
    perror("setsockopt: ");
    return -1;
  }
  return 0;
}


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-o x: Port number                                  -> Default: " << occurence << std::endl;
  std::cout << "-n x: Number of packets                            -> Default: infinite" << std::endl;
  std::cout << "-p  : Print packets                                -> Default: FALSE" << std::endl;
  std::cout << "-c  : Enable data check                            -> Default: FALSE" << std::endl;  
  std::cout << "  Modifiers of -c option:" << std::endl;
  std::cout << "  -L: Enable checking that L1ids are consecutive   -> Default: FALSE" << std::endl;  
  std::cout << "-f x: Write data to file. The parameter is the path and the name of the file" << std::endl;
  std::cout << "-C x: Compare with data from file. The parameter is the path and the name of the reference file" << std::endl;
  std::cout << "-v  : Verbose output                               -> Default: FALSE" << std::endl;
  std::cout << std::endl;
}

/***************************/
void printStat(PrintType arg) 
/***************************/
{
  std::cout << std::endl << std::endl << "----------Test Statistics-----------" << std::endl;

  if (arg) 
  {
    std::cout << " # packets transferred       = " << (ipacket - lpacket) << std::endl;
    std::cout << " # corrupted packets         = " << errors[GLOBAL] << std::endl;
    std::cout << "      wrong size             = " << errors[SIZE] << std::endl;
    std::cout << "      wrong L1id             = " << errors[L1ID] << std::endl;
    std::cout << " time elapsed (s)            = " << delta_time << std::endl;
    std::cout << "------------------------------------" << std::endl;
  }
  
  std::cout << " Total # packets transferred = " << (ipacket-1) << std::endl;
  std::cout << " Total # corrupted packets   = " << errorsTot[GLOBAL] << std::endl;
  std::cout << "            wrong size       = " << errorsTot[SIZE] << std::endl;
  std::cout << "            wrong L1id       = " << errorsTot[L1ID] << std::endl;
  
  if (arg && (ipacket - lpacket)) 
  {    
    t_per_packet = ((float) delta_time * 1000000) / (ipacket - lpacket);
    packet_per_s = (float)1000000 / t_per_packet;
    mb_per_s = ((float)(ipacket - lpacket) * last_packet_size) / ((float)delta_time * 1024 * 1024);
    std::cout << "------------------------------------" << std::endl;
    std::cout << " time/packet = " << t_per_packet << " microseconds" << std::endl;
    std::cout << " packet/s    = " << packet_per_s << " packet/s" << std::endl;
    std::cout << " Mbyte/s     = " << mb_per_s << std::endl;
  }

  std::cout << "------------------------------------" << std::endl << std::endl;
}


/*****************************/
void errorFound(CheckType type) 
/*****************************/
{
  if (!flg_error) 
  {
    errors[GLOBAL]++;
    errorsTot[GLOBAL]++;
  }
  flg_error = 1;
  errors[type]++;
  errorsTot[type]++;
  test_status = TEST_NOT_OK;
}


/********************/
int terminate_it(void) 
/********************/
{
  // Close the ethernet device
  std::cout << std::endl << std::endl <<"Closing network link ..." << std::endl;
  if(close(fd) == -1)
  {
    perror("close: ");
    exit (TEST_NOT_OK);
  }
  
  if (writefile)
    close (outputFile);
    
  printStat(SUMMARY);
  return(test_status);
}


// sigquit handler
/**********************************/
void sigquit_handler(int /*signum*/)
/**********************************/
{
  int i;
  std::cout << "Latest L1ID = " << last_l1id << std::endl;
  if (start_time) 
  {
    time(&end_time);
    delta_time = end_time - start_time;
    if (delta_time) 
      printStat(FULL);
  }
  
  // reset flags
  lpacket = ipacket;
  for (i = 0; i < GLOBAL; i++) 
    errors[i] = 0;
  time(&start_time);
}


// sigint handler
/*********************************/
void sigint_handler(int /*signum*/)
/*********************************/
{
  exit(terminate_it());
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  MemoryPage *mem_page;
  MemoryPool *m_memoryPool;
  ROBFragment *robfragment;
  char buf[4];                  // place to store the msg size
  struct sigaction sa;
  struct sigaction saint;
  int code = 0;
  int c;
  int eth_block_size;
  int msg_size, nread;
  unsigned int nrecv;
  unsigned int virt_addr;

  while ((c = getopt(argc, argv, "ho:n:pcLvf:C:")) != -1)
    switch (c) 
    {
      case 'h':
        std::cout << "Usage: " << argv[0] << " [options]: "<< std::endl;
        usage();
        exit(TEST_UNRESOLVED);
        break;

      case 'n':
        npackets = atoi(optarg);
        if (npackets<0) 
        {
	  std::cout << "Number of packets must be positive" << std::endl;
	  exit(TEST_UNRESOLVED);
        } 
        break;
                  
      case 'f':   
        writefile = TRUE;
        sscanf(optarg,"%s",filename);
        std::cout << "writing data to file " << filename << std::endl;
        break;
            
      case 'C':   
        checkfile = TRUE;
        sscanf(optarg, "%s", filename2);
        std::cout << "Reading reference fragment from file " << filename2 << std::endl;
        break;
      
      case 'p': print = TRUE;                      break;
      case 'c': dcheck = TRUE;                     break;
      case 'L': L1extdcheck = TRUE;                break;
      case 'v': verbose = TRUE;                    break;
      case 'o': occurence = atoi(optarg);          break;

      default:
        std::cout << "Invalid option " << c << std::endl;
        std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
        usage();
        exit (TEST_UNRESOLVED);
    }
  
  if (verbose) 
    std::cout << "Creating socket.." << std::endl;

  // Set up the local address
  bzero((char *)&sockin, sizeof(sockin));
  sockin.sin_family      = AF_INET;
  sockin.sin_addr.s_addr = htonl(INADDR_ANY);
  sockin.sin_port        = occurence;
  sockin.sin_port        = htons(sockin.sin_port);
  
  if (verbose)
    std::cout << "Address family = " << sockin.sin_family  << ", Address = " << sockin.sin_addr.s_addr <<
            ", Address port = " << sockin.sin_port << "(" << std::hex << sockin.sin_port << std::dec << ")" << std::endl;
            
  if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
  {
    perror("socket: ");
    exit(-1);
  }
  if (SetSocketOptions(fd))
    exit(-1);
    
  // Bind local address
  if (verbose) 
    std::cout << "..binding.." << std::endl;
  if (bind(fd, (struct sockaddr *)&sockin, sizeof(sockin)) == -1)
  {
    perror("bind: ");
    exit(-1);
  }
    
  /* Listen for connections on local address */
  if (verbose) 
    std::cout << "..listening.." << std::endl;
  if (listen (fd, SOMAXCONN) == -1)
  {
    perror("listen: ");
    exit(-1);
  }

  if (verbose) 
   std::cout << "..and accepting.." << std::endl;
  slen = (socklen_t)sizeof(struct sockaddr_in);
  fd = accept(fd, (struct sockaddr *)&sout, &slen);
  if (fd == -1)
  {
    perror("accept failed ");
    exit(-1);
  }

  if (verbose) 
    std::cout << "TCP connection opened locally" << std::endl;
 
  //Create the Memory Pool
  m_memoryPool = new MemoryPool_CMEM(10, MAX_PACKET_SIZE_WRDS * sizeof(int));
   
  //Get a page
  try
  {
    mem_page = m_memoryPool->getPage();  //MJ: Check for error!!
  }
  catch (MemoryPoolException& e)
  {
    std::cout << e;
  }


  virt_addr = (u_long) mem_page->address() + sizeof(ROBFragment::ROBHeader);
  page_size = mem_page->capacity();

  if (verbose) 
  {
    std::cout << "Printing buffer parameters:" << std::endl; 
    std::cout << "Virtual address of buffer  = 0x" << std::hex << virt_addr << std::dec << std::endl;
    std::cout << "Buffer size in bytes       = " << mem_page->capacity() << std::endl;
  }
    
  // Install signal handler for SIGQUIT & SIGINT 
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with " << std::endl;
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TEST_UNRESOLVED);
  }

  // Install signal handler for SIGINT 
  sigemptyset(&saint.sa_mask);
  saint.sa_flags   = 0;
  saint.sa_handler = sigint_handler;

  if (sigaction(SIGINT, &saint, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with " << std::endl;
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TEST_UNRESOLVED);
  }
  
  // Open the file for output
  if (writefile)
  {
    outputFile = open(filename, O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
    if (outputFile < 0)
    {
      std::cout << "Failed to open " << filename << std::endl;
      exit(-1);
    }
  }
      
  // Open the reference file
  if (checkfile)
  {
    inputFile = open(filename2, O_RDONLY);
    if (inputFile < 0)
    {
      std::cout << "Failed to open " << filename2 << std::endl;
      exit(-1);
    }

    int refsize = read(inputFile, refbuf, 0x100000);
    std::cout << "The reference file contains " << refsize << " bytes" << std::endl;
    if (refsize < 44)
    {
      std::cout << "The reference file contains less than 44 bytes (i.e. ROD header + trailer)" << std::endl;
      exit(-1);
    }
    if (refsize == 0x100000)
    {
      std::cout << "The reference file contains more than 1 MB" << std::endl;
      exit(-1);
    }
    close(inputFile);
  }
    
  time(&start_time);
  lpacket = 1;

  std::cout << std::endl << "Press ctrl-\\ to output statistics" << std::endl;
  std::cout << "Press ctrl-c to quit" << std::endl << std::endl; 

  ipacket = 1;
  while(ipacket <= ((npackets >= 0) ? npackets : (ipacket))) 
  {   
    // First read the size of the message
    nrecv = 0;
    if (verbose)
      std::cout << "Waiting for fragment " << ipacket << std::endl;
    while(nrecv < sizeof(int)) 
    {
      nread = read(fd, buf + nrecv, sizeof(int) - nrecv);  
      if(nread <= 0) 
      {
        if(errno != EINTR) 
        {
	  std::cout << "Unable to read size of incoming message" << std::endl;
	  exit(-1);
        }
      }
      else 
        nrecv += nread;
    }

    // Get the size of the ROD fragment
    msg_size = (int)ntohl(*((unsigned long*)buf));

    if (page_size < msg_size)
    {
      eth_block_size = page_size;
      std::cout << "ROD fragment will be truncated" << std::endl;
    }
    else
      eth_block_size = msg_size;

    if (verbose)
      std::cout << "There is a fragment of " << eth_block_size << " bytes to be received" << std::endl; 

    packet_size = 0;
    while(packet_size < eth_block_size) 
    {
      nread = read(fd, (void *)(virt_addr + packet_size), eth_block_size - packet_size);
      if (nread <= 0) 
      {
        if (errno != EINTR) 
        {
	  std::cout << "Unable to read incoming message" << std::endl;
	  exit(-1);
        }
      }
      else 
        packet_size += nread;
    }
    
    if (verbose)
      std::cout << "Packet of "  << packet_size << " bytes received" << std::endl; 
    last_packet_size = packet_size;
    
    // Create a ROB fragment
    robfragment = new ROBFragment(mem_page, packet_size / 4, 0, 0);
    last_l1id = robfragment->level1Id();

    if (print) 
      std::cout << *robfragment;    

    if (writefile)
    {
      const Buffer* buffer = robfragment->buffer();
      Buffer::page_iterator page = buffer->begin(); 
      if (verbose)
        std::cout << "writing " << packet_size * 4 << " bytes to file" << std::endl; 

      u_int tmpdata = (u_long)((*page)->address());
      tmpdata += sizeof(ROBFragment::ROBHeader);
      int isok = write(outputFile, (void *)tmpdata, packet_size * 4);
      if (isok < 0)
      {
        std::cout << "Error in writing to file" << std::endl;
        exit(-1);
      }
    }
      
    // data check
    if (dcheck) 
    { 
     flg_error = 0;
     ccode = (CheckType) robfragment->check(ipacket, L1extdcheck);
     if (code && verbose)
       std::cout << "Check of ROD fragment returns error " << ccode << std::endl;
     if (ccode)
       errorFound(ccode);
    }
      
    if (checkfile)
    {
      unsigned int *refptr = (unsigned int *)refbuf;
      unsigned int *dataptr = (unsigned int *)virt_addr;
      for (int dataword = 0; dataword < packet_size; dataword++)
      {
        if (*refptr != *dataptr)
          std::cout << "Data mismatch: Offset = " << std::hex << dataword * 4 << " Received data = " << *dataptr << "   Expected data = " << *refptr << std::dec << std::endl;
        refptr++;
        dataptr++;
      }
    }
      
    ipacket++;

    delete robfragment;
  }
  return(terminate_it());  
}



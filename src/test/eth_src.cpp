/*****************************************************************************/
/*                                                                           */
/* File Name        : eth_src.cpp                                            */
/* Version          : 1.0                                                    */
/* Author           : Markus Joos (based on slink_src_dvs.c by B. Gorini)    */
/*                                                                           */
/*****************************************************************************/

#include <cstdlib>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <iostream>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>  // struct sockaddr_in ...
#include <netinet/tcp.h> // TCP_NODELAY
#include <sys/socket.h>
#include <arpa/inet.h>
#include "rcc_error/rcc_error.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSEventFragment/RODFragment.h"

using namespace ROS;

// Standard header and trailer sizes
#define HEADER_SIZE_WRDS        (sizeof(RODFragment::RODHeader) / 4)
#define TRAILER_SIZE_WRDS       (sizeof(RODFragment::RODTrailer) / 4)
#define STATUS_SIZE             1
#define STATUS_SIZE_SLIDAS      3

// Minimum packet size is: header + trailer words + at least 4 words
#define MIN_PACKET_SIZE_WRDS    (HEADER_SIZE_WRDS + TRAILER_SIZE_WRDS + 4)
#define MAX_PACKET_SIZE_WRDS    2048

#define MEM_POOL_NUMB           10                           //Max. number of ROD fragments
#define MEM_POOL_SIZE           (MAX_PACKET_SIZE_WRDS * 4)   //Max. size of ROD fragments

#define DMA_CHAIN_LENGTH 200
#define MAX_PORT 10

// output codes (for dvs)
#define TEST_OK                 0
#define TEST_NOT_OK             183
#define TEST_UNRESOLVED         184

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif

// Global variables
int print = FALSE;
unsigned int size = 256;
unsigned int maxsize = MAX_PACKET_SIZE_WRDS;
unsigned int minsize = MIN_PACKET_SIZE_WRDS;
int fd;
int wait = 0 ;
int npackets = -1;
char rob_ip[128];
short port = 10000;    
int flg_random = FALSE;
int flg_rodemul = TRUE;
int flg_size = FALSE;
int flg_minsize = FALSE;
int flg_maxsize = FALSE;
int flg_verbose = FALSE;
int first_l1id = 0;	
int d_size;
int level1Id;
unsigned int module_id = 0;
unsigned int crate_id = 0;
unsigned int detector_id = 0xa0;
unsigned int l1_type = 0x7 ;
unsigned int det_type = 0xa ; 
char dummy[7];


/*********************************/
static int SetSocketOptions(int fd)
/*********************************/
{
  int sockopt = 0;
  int SOCKET_ERROR = -1;
  static const int c_so_rcvbuf = 256 * 1024; // limit set by Linux!
  static const int c_so_sndbuf = 256 * 1024; // limit set by Linux!

  std::cout << "c_so_rcvbuf = " << c_so_rcvbuf << std::endl;

  // setsockopt SO_REUSEADDR - enable/disable local address reuse 
  sockopt = 1;
  if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char*)&sockopt, sizeof(sockopt)) == SOCKET_ERROR)
  {
    perror("setsockopt: ");
    return (-1);
  }

  // setsockopt SO_SNDBUF - set buffer size for output 
  sockopt = c_so_sndbuf;
  if (setsockopt(fd, SOL_SOCKET, SO_SNDBUF, (char*)&sockopt, sizeof(sockopt)) == SOCKET_ERROR)
  {
    perror("setsockopt: ");
    return (-1);
  }
 
  // setsockopt SO_RCVBUF - set buffer size for input 
  sockopt = c_so_rcvbuf;
  if (setsockopt(fd, SOL_SOCKET, SO_RCVBUF, (char*)&sockopt, sizeof(sockopt)) == SOCKET_ERROR)
  {
    perror("setsockopt: ");
    return (-1);
  }

  // setsockopt TCP_NODELAY - to defeat the packetization algorithm 
  sockopt = 0; // 0 to enable Nagle algorithm
  if (setsockopt(fd, getprotobyname("tcp")->p_proto, TCP_NODELAY, (char *)&sockopt, sizeof(sockopt)) == SOCKET_ERROR)
  {
    perror("setsockopt: ");
    return (-1);
  }

  return 0;
}


/********************/
int terminate_it(void) 
/********************/
{
  // Close the ethernet device
  if(close(fd) == -1)
  {
    perror("close: ");
    exit (TEST_NOT_OK);
  }

  return TEST_OK;
}


// ctrl-\ handler
/******************************/
void sigquit_handler(int signum)
/******************************/
{
  std::cout << "L1ID of last event = " << level1Id << std::endl;
}


// sigint handler
/*****************************/
void sigint_handler(int signum)
/*****************************/
{
  exit(terminate_it());
}

/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-i x: ROB IP address                               -> Default: " << rob_ip << std::endl;
  std::cout << "-o x: Ethernet port number                         -> Default: " << port << std::endl;
  std::cout << "-S  : SLIDAS emulation (otherwise simulate standard ROD)" << std::endl;
  std::cout << "  Modifiers of -S option:" << std::endl;
  std::cout << "  -O x: Module id (Hex format)                     -> Default: 0x" << std::hex << module_id << std::dec << std::endl;
  std::cout << "  -D x: Detector id (Hex format)                   -> Default: 0x" << std::hex << detector_id << std::dec << std::endl;
  std::cout << "  -L x: L1 Trigger type (Hex format)               -> Default: 0x" << std::hex << l1_type << std::dec << std::endl;
  std::cout << "  -T x: Detector event type (Hex format)           -> Default: 0x" << std::hex << det_type << std::dec << std::endl;
  std::cout << "-f x: First L1id for ROD emulation                 -> Default: " << first_l1id << std::endl;
  std::cout << "-s x: Packet size in words (incompatible with -r)  -> Default: " << size << std::endl;
  std::cout << "-r  : Random packet size (incompatible with -s)" << std::endl;
  std::cout << "  Modifiers of -r option:" << std::endl;
  std::cout << "  -M x: Max packet size                            -> Default: " << maxsize << std::endl;
  std::cout << "  -m x: Min packet size                            -> Default: " << minsize << std::endl;
  std::cout << "-w x: Wait before sending (in seconds)             -> Default: " << wait << std::endl;
  std::cout << "-n x: Number of packets                            -> Default: infinite loop" << std::endl;
  std::cout << "-p  : Print packets" << std::endl;
  std::cout << "-v  : Verbose output" << std::endl;
  std::cout << std::endl;
}

/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  ROBFragment *robfragment;
  MemoryPool* mem_pool;
  Buffer* fragbuffer;
  char* virt_addr = 0;
  unsigned int code;
  unsigned int bytes_sent;
  unsigned int nbytes = 0;
  int bytes = 0;   
  unsigned int f_size, d_size, numberOfRODStatusElements;
  unsigned int numberOfDataElements, statusBlockPosition, bunchCrossingId, sourceId;
  struct sockaddr_in sout;
  struct sigaction sa;
  struct sigaction saint;
  int ipackets, c;
  int bcid = 0;
  int network_size;
  //Pointers to access data

  strcpy(rob_ip, "192.168.100.27");

  while ((c = getopt(argc, argv,"ho:i:SO:D:L:T:f:s:rM:m:w:n:pv")) != -1)
    switch (c) 
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
      usage();
      exit(TEST_UNRESOLVED);
      break;

    case 's':
      flg_size = TRUE;
      size = atoi(optarg);
      if (size < MIN_PACKET_SIZE_WRDS || size > MAX_PACKET_SIZE_WRDS) 
      {
	std::cout << "Wrong packet size: " << size << std::endl << "(acceptable range: " << MIN_PACKET_SIZE_WRDS 
             << " - " << MAX_PACKET_SIZE_WRDS << ")" << std::endl;
	exit(TEST_UNRESOLVED) ;
      }
      break;

    case 'i': strcpy(rob_ip, optarg);            break;
    case 'S': flg_rodemul = FALSE;               break;
    case 'O': sscanf(optarg,"%x", &module_id);   break;
    case 'D': sscanf(optarg,"%x", &detector_id); break;
    case 'L': sscanf(optarg,"%x", &l1_type);     break;
    case 'T': sscanf(optarg,"%x", &det_type);    break;
    case 'f': first_l1id = atoi(optarg);         break;
    case 'w': wait = atoi(optarg);               break;
    case 'p': print = TRUE;                      break;
    case 'v': flg_verbose = TRUE;                break;
    case 'o': port = atoi(optarg);               break;
      
    case 'r':  
      flg_random = TRUE;
      //init random generation
      srand((unsigned int)time(NULL));
      break;

    case 'M':
      flg_maxsize = TRUE;
      maxsize = atoi(optarg);
      if (maxsize > MAX_PACKET_SIZE_WRDS) 
      {
	std::cout << "Wrong max packet size: " << size << std::endl 
             << "(must be < "  << MAX_PACKET_SIZE_WRDS << ")" << std::endl;
	exit(TEST_UNRESOLVED) ;
      }
      break;
 
    case 'm':
      flg_minsize = TRUE;
      minsize = atoi(optarg);
      if (minsize < MIN_PACKET_SIZE_WRDS) 
      {
	std::cout << "Wrong max packet size: " << size << std::endl 
             << "(must be < "  << MAX_PACKET_SIZE_WRDS << ")" << std::endl;
	exit(TEST_UNRESOLVED) ;
      }
      break;
 
    case 'n':
      npackets = atoi(optarg);
      if (npackets<0) 
      {
	std::cout << "number of packets must be positive" << std::endl;
	exit(TEST_UNRESOLVED);
      } 
      break;

    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << "[options]" << std::endl;
      usage();
      exit (TEST_UNRESOLVED);
    }

  //Check compatibility of options
  if (flg_random && flg_size) 
  { 
    std::cout << "Options -s and -r are incompatible!" << std::endl;
    exit (TEST_UNRESOLVED);
  }
  
  if ( (flg_minsize||flg_maxsize) && !flg_random) 
  {
    std::cout << "-m and -M options can only be used together with -r option" << std::endl;
    exit (TEST_UNRESOLVED);
  }

  bzero((char *)&sout, sizeof(sout));
  sout.sin_family = AF_INET;
  sout.sin_addr.s_addr = inet_addr(rob_ip);
  sout.sin_port = htons(port);

  if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
  {
    perror("socket: ");
    exit (-1);
  }
  
  if(SetSocketOptions(fd))
  {
    std::cout << "Call to SetSocketOptions failed" << std::endl; 
    exit (-1);
  }
  
  if (connect(fd, (struct sockaddr *)&sout, sizeof(sout)) == -1) 
  {
    perror("connect: ");
    exit (-1);
  }

  if (flg_verbose) 
  {
    std::cout << "Ethernet Parameters:" << std::endl;
    std::cout << "IP address:          " << rob_ip << std::endl;
    std::cout << "Port number:         " << port << std::endl;
  }

  //wait some time to be sure that the destination has started
  if (wait > 0)
    sleep(wait);
  if (wait < 0)
  {
    std::cout << "Press return to start sending data" << std::endl;
    fgets(dummy, 7, stdin);
  }
  
  // Install signal handler for SIGQUIT
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with " << std::endl;
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit (TEST_UNRESOLVED);
  }

  // Install signal handler for SIGINT
  sigemptyset(&saint.sa_mask);
  saint.sa_flags   = 0;
  saint.sa_handler = sigint_handler;

  if (sigaction(SIGINT, &saint, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with " << std::endl;
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TEST_UNRESOLVED);
  }

  std::cout << std::endl <<"Press ctrl-\\ to output statistics" << std::endl;
  std::cout << "Press ctrl-c to quit" << std::endl << std::endl;

  //Create a memory pool
  if (flg_verbose) 
  {
    std::cout << "Creating MemoryPool_CMEM" << std::endl;
    std::cout << "Number of pages = " << MEM_POOL_NUMB << std::endl;
    std::cout << "Page size = " << std::hex << MEM_POOL_SIZE << " bytes" << std::dec << std::endl;
  }

  try
  {
    mem_pool = new MemoryPool_CMEM(MEM_POOL_NUMB, MEM_POOL_SIZE);
  }
  catch (MemoryPoolException& e)
  {
    std::cout << e << std::endl;
    exit(TEST_UNRESOLVED);
  }

  level1Id = first_l1id - 1;
  int running = 1;
  while(running)
  {
    // Event loop
    for (ipackets = 1; npackets == -1 || ipackets <= npackets ; ipackets++) 
    {
      // Generate the L1ID  
      level1Id++;
      if (!flg_rodemul) // wrap a la SLIDAS
        level1Id &= 0xffff;

      // Generate the Source ID
      sourceId = (module_id & 0xff) | ((detector_id & 0xff) << 8);  //module type = 0 (ROD)

      // Generate the BCID a la SLIDAS ... yes I know its wrong  
      if (ipackets == 1) 
        bunchCrossingId = bcid = 0;
      else 
        bunchCrossingId = (bcid += 3) & 0xfff;

      if (!flg_rodemul)
        numberOfRODStatusElements = STATUS_SIZE_SLIDAS;
      else
        numberOfRODStatusElements = STATUS_SIZE;

      // Generate the data size
      if (flg_random) 
      {
        d_size = rand() % (maxsize - minsize + 1);  // generate size    
        if (flg_verbose)
          std::cout << "Random fragment size = " << d_size << std::endl;
      }
      else
      {
        d_size = size - HEADER_SIZE_WRDS - TRAILER_SIZE_WRDS - numberOfRODStatusElements;
        if (flg_verbose)
          std::cout << "fixed fragment size = " << d_size << std::endl;
      }

      numberOfDataElements = d_size;
      statusBlockPosition = 0;

      // Create an emulated ROB fragment
      // I know that we need a ROD fragment but the ROSEventFragment package
      // should not have to deal with ROD fragments
      try
      {
      robfragment = new ROBFragment(mem_pool, sourceId, level1Id, bunchCrossingId, l1_type, 
                                    det_type, numberOfRODStatusElements, numberOfDataElements,
                                    statusBlockPosition);
      }
      catch (ROSException& e)
      {
        std::cout << "ROSException from call to (new ROBFragment>:" << std::endl;
        std::cout << e << std::endl;
      }
      catch(...)
      {
        std::cout << "Exception received from <new ROBFragment>" << std::endl;
        exit(-1);
      }

      // Fill the data block
      robfragment->fill_data();  

      // Make some modifications to the ROD fragment
      if (!flg_rodemul) 
        robfragment->emulate_SLIDAS();

      // Print packet
      if (print)
      {
        std::cout << "Printing packet" << std::endl;
        std::cout << *robfragment;
        std::cout << std::endl;
      }

      //Write packet to the network 
      //first send the size of the outgoing fragment 
      f_size = 4 * (d_size + HEADER_SIZE_WRDS + TRAILER_SIZE_WRDS + numberOfRODStatusElements);
      network_size = htonl(f_size);
      bytes_sent = write(fd, (char*)&network_size, sizeof(int));
      if(bytes_sent != sizeof(int)) 
      {
        std::cout << "Size of fragment not properly sent!" << std::endl;
        std::cout << "bytes_sent = " << bytes_sent << " (0x" << std::hex << bytes_sent << std::dec << ")" << std::endl;
        perror("write: ");
        exit (-1);
      }

      if (flg_verbose) 
        std::cout << "Size = " << f_size << " written for L1ID = "<< level1Id << std::endl;

      fragbuffer = robfragment->buffer();
      Buffer::page_iterator i = fragbuffer->begin();
      virt_addr = (char*)(*i)->address() + sizeof(ROBFragment::ROBHeader);
      nbytes = 0;
      while(nbytes < f_size) 
      { 
        bytes = write(fd, virt_addr + nbytes, f_size - nbytes);
        if (bytes >= 0) 
        nbytes += bytes;
        else if((bytes < 0) && (errno != EINTR)) 
        {
          perror("write: ");
          exit (-1);
        }
      }

      if (flg_verbose) 
        std::cout << "Data written for L1ID = " << level1Id << std::endl;

      //Remove the ROB Fragment
      delete robfragment;
    }
    std::cout << "Do you want to continue? (1=yes  0=no):  " << std::endl;
    std::cin >> running;
    if (running)
    {
      std::cout << "Enter the number of events (-1=run forever):  " << std::endl;
      std::cin >> npackets;
      int mode;
      std::cout << "Reset L1ID? (1=yes  0=no):  " << std::endl;
      std::cin >> mode;
      if (mode)
        level1Id = 0;        
    }
  }
  
  std::cout << "Press <return> to terminate the program" << std::endl;
  return(terminate_it()) ;
}


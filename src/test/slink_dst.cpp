/*****************************************************************************/
/*                                                                           */
/* File Name        : slink_dst.cpp                                          */
/*                                                                           */
/* Author           : B.Gorini (based on slink_dst.c!)                       */
/*									     */
/* Version          : 1.3 - 5/11/2002 - M. Joos :			     */ 
/*                      ported to new ROS S/W. PPC support removed	     */
/* Version          : 1.2 - 19/3/2002 - B.Gorini :                           */
/*                      added check that L1Id is sequencial (with -c option) */
/* Version          : 1.1 - 7/8/2001 - B.Gorini :                            */
/*                      included synchronization mechanism through pmg       */
/* Version          : 1.0 - 15/7/2001 - B.Gorini :                           */
/*                      initial release                                      */
/*                                                                           */
/*****************************************************************************/

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <exception>
#include <getopt.h>
#include "rcc_error/rcc_error.h"
#include "ROSslink/slink.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSEventFragment/ROBFragment.h"
#include "DFDebug/DFDebug.h"
#include "rcc_time_stamp/tstamp.h"
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>
#include <pmg/pmg_initSync.h>

using namespace ROS;
using namespace daq::tmgr;

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif

// global variables

int dma = TRUE;
int verbose = FALSE;
int print = FALSE;
int dcheck = FALSE;
int L1extdcheck = FALSE; 
int dataSwap = TRUE;
int cwordSwap = TRUE;
int npackets = -1;
int occurence = 1 ;
int test_status = TmPass;
int writefile = FALSE;
int usdelay = 0;
int dblevel = 0;
int outputFile;
char filename[200] = {0};
int checkfile = FALSE;
int inputFile;
char filename2[200] = {0};
int flg_error, ipacket, lpacket, delta_time, page_size, packet_size;
long start_time,end_time;
float t_per_packet, packet_per_s, mb_per_s;
unsigned int rec_size,exp_size, last_l1id = 999999;
char refbuf[0x100000];
bool DVS = FALSE;

typedef enum printType { SUMMARY=0 , FULL=1 } PrintType;
typedef enum checkType { SIZE , L1ID , GLOBAL } CheckType;

int errors[GLOBAL+1] = { GLOBAL * 0 , 0 };
int errorsTot[GLOBAL+1] = { GLOBAL * 0 , 0 };
CheckType ccode;

void push_run (SLINK_device *dev);

// statics used in main and sigint handler
static SLINK_device *dev;

// Constants
#define MAX_PACKET_SIZE_WRDS 2048 

/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-o x: Occurence                                       -> Default: " << occurence << std::endl;
  std::cout << "-n x: Number of packets                               -> Default: infinite" << std::endl;
  std::cout << "-D  : Do not use dma                                  -> Default: FALSE (i.e. use DMA)" << std::endl;
  std::cout << "-p  : Print packets                                   -> Default: FALSE" << std::endl;
  std::cout << "-c  : Enable data check                               -> Default: FALSE" << std::endl;  
  std::cout << "-s  : Disable data swap                               -> Default: FALSE" << std::endl;	
  std::cout << "-S  : Disable control word swap                       -> Default: FALSE" << std::endl;	
  std::cout << "Modifiers of -c option:" << std::endl;
  std::cout << "  -L: Enable checking that L1ids are consecutive      -> Default: FALSE" << std::endl;  
  std::cout << "-f x: Write data to file. The parameter is the" << std::endl;
  std::cout << "      path and the name of the file" << std::endl;
  std::cout << "-C x: Compare with data from file. The parameter" << std::endl;
  std::cout << "      is the path and the name of the reference file" << std::endl;
  std::cout << "-w x: Number of microseconds to wait after" << std::endl;
  std::cout << "      the receptionof a packet                        -> Default: 0" << std::endl;
  std::cout << "-e x: Debug level (ROSEBUG)                           -> Default: 0 (disabled" << std::endl;
  std::cout << "-v  : Verbose output                                  -> Default: FALSE" << std::endl;
  std::cout << "--DVS : To be used by DVS                             -> Default: FALSE" << std::endl;
  std::cout << std::endl;
}


/***************************/
void printStat(PrintType arg) 
/***************************/
{
  std::cout << std::endl << std::endl << "----------Test Statistics-----------" << std::endl;

  if (arg) 
  {
    std::cout << " # packets transferred       = " << (ipacket - lpacket) << std::endl;
    std::cout << " # corrupted packets         = " << errors[GLOBAL] << std::endl;
    std::cout << "      wrong size             = " << errors[SIZE] << std::endl;
    std::cout << "      wrong L1id             = " << errors[L1ID] << std::endl;
    std::cout << " time elapsed (s)            = " << delta_time << std::endl;
    std::cout << "------------------------------------" << std::endl;
  }
  
  std::cout << " Total # packets transferred = " << (ipacket-1) << std::endl;
  std::cout << " Total # corrupted packets   = " << errorsTot[GLOBAL] << std::endl;
  std::cout << "            wrong size       = " << errorsTot[SIZE] << std::endl;
  std::cout << "            wrong L1id       = " << errorsTot[L1ID] << std::endl;
  
  if (arg && (ipacket - lpacket)) 
  {    
    t_per_packet = ((float) delta_time * 1000000) / (ipacket - lpacket);
    packet_per_s = (float)1000000 / t_per_packet;
    mb_per_s = ((float)(ipacket - lpacket) * packet_size * 4) / ((float)delta_time * 1024 * 1024);
    std::cout << "------------------------------------" << std::endl;
    std::cout << " time/packet = " << t_per_packet << " microseconds" << std::endl;
    std::cout << " packet/s    = " << packet_per_s << " packet/s" << std::endl;
    std::cout << " Mbyte/s     = " << mb_per_s << std::endl;
  }

  std::cout << "------------------------------------" << std::endl << std::endl;
}


/*****************************/
void errorFound(CheckType type) 
/*****************************/
{
  if (!flg_error) 
  {
    errors[GLOBAL]++ ;
    errorsTot[GLOBAL]++ ;
  }
  flg_error = 1 ;
  errors[type]++ ;
  errorsTot[type]++ ;
  test_status = TmFail ;
}


/********************/
int terminate_it(void) 
/********************/
{
  err_type code;

#if defined(SLIDAS_PUSH) 
  // stop run on SLIDAS  
  SLIDAS_PushRun(dev);
#endif

  std::cout << std::endl << std::endl <<"Closing S-link ..." << std::endl;
  SLINK_PrintState(dev);
  SSP_Reset(dev);
  code = SLINK_SSPClose(dev);
  if (code)
  {
    std::cout << "main: SLINK_SSPClose() FAILED with error " << std::hex << code << std::endl;
    rcc_error_print(stdout, code);
  }
  
  if (writefile)
    close (outputFile);
  
  code = ts_close(TS_DUMMY);
  if (code) 
    rcc_error_print(stdout, code);  
  
  printStat(SUMMARY);
  return(test_status);
}


// sigquit handler
/******************************/
void sigquit_handler(int signum)
/******************************/
{
  int i;
  
  SLINK_PrintState(dev);
  if (start_time) 
  {
    time(&end_time);
    delta_time = end_time - start_time;
    if (delta_time) 
      printStat(FULL);
  }
  
  // reset flags
  lpacket = ipacket;
  for (i = 0;i < GLOBAL; i++) {errors[i] = 0;}
  time(&start_time);
}


// sigint handler
/*****************************/
void sigint_handler(int signum)
/*****************************/
{
  exit(terminate_it());
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  MemoryPage *mem_page;
  MemoryPool *m_memoryPool;
  ROBFragment *robfragment;
  ReturnCode code;
  SLINK_parameters slink_parameters;
  char *s_addr;
  struct sigaction sa, saint;
  int c, status;
  unsigned int virt_addr, pci_addr;

  static struct option long_options[] = {"DVS", no_argument, NULL, '1'};
  
  try 
  {
    while ((c = getopt_long(argc, argv, "ho:n:DpcLvsSf:C:w:e:1", long_options, NULL)) != -1)
      switch (c) 
      {
      case 'h':
        std::cout << "Usage: " << argv[0] << " [options]: "<< std::endl;
        usage();
        exit(TmUnresolved);
        break;

      case 'o':
        occurence = atoi(optarg);
        if (occurence <= 0 || occurence >= MAX_DEVICES)
        { 
	  std::cout << "Occurence exceeds allowed bounds" << std::endl;
          exit(TmUnresolved);
        }
        break;

      case 'n':
        npackets = atoi(optarg);
        if (npackets < 0) 
        {
	  std::cout << "number of packets must be positive" << std::endl;
	  exit(TmUnresolved);
        } 
        break;
                  
    case 'f':   
      writefile = TRUE;
      sscanf(optarg, "%s", filename);
      std::cout << "Writing data to file " << filename << std::endl;
      break;
      
     case 'C':   
      checkfile = TRUE;
      sscanf(optarg, "%s", filename2);
      std::cout << "Reading reference fragment from file " << filename2 << std::endl;
      break;
      
      case 'w': usdelay = atoi(optarg);   break;                   
      case 'D': dma = FALSE;              break;
      case 'p': print = TRUE;             break;
      case 'c': dcheck = TRUE;            break;
      case 's': dataSwap = FALSE;         break;
      case 'S': cwordSwap = FALSE;        break;
      case 'L': L1extdcheck = TRUE;       break;
      case 'v': verbose = TRUE;           break;
      case 'e': dblevel = atoi(optarg);   break;
      case '1': DVS = TRUE;               break;
		
      default:
        std::cout << "Invalid option " << c << std::endl;
        std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
        usage();
        exit (TmUnresolved);
      }
  
    code = ts_open(0, TS_DUMMY);
    if (code) 
    {
      rcc_error_print(stdout, code);
      exit (TmFail);
    }
    
    DF::GlobalDebugSettings::setup(dblevel, DFDB_SLINK);

    code = SLINK_Open();
    if (code) 
    {
      rcc_error_print(stdout, code);
      exit (TmFail);
    }

    // Initializes the SLINK Structure
    SLINK_InitParameters(&slink_parameters);

    // Overwrite selected default parameters
    slink_parameters.occurence = occurence;

    if (dataSwap == TRUE) 
       slink_parameters.byte_swapping = SLINK_ENABLE;
    else 
       slink_parameters.byte_swapping = SLINK_DISABLE;
     
    if (cwordSwap == TRUE)  
    {
      slink_parameters.start_word = 0x0000F0B0;
      slink_parameters.stop_word  = 0x0000F0E0;
    }
    else 
    {
      slink_parameters.start_word = 0xB0F00000;
      slink_parameters.stop_word  = 0xE0F00000;
    }

    if (verbose) 
    {
      std::cout << "slink_parameters structure : " << std::endl;
      std::cout << "data_width    = " << slink_parameters.data_width << std::endl;
      std::cout << "occurence     = " << slink_parameters.occurence << std::endl;
      std::cout << "timeout       = " << slink_parameters.timeout << std::endl;
      std::cout << "byte_swapping = " << slink_parameters.byte_swapping << std::endl;
      std::cout << "start_word    = 0x" << std::hex << slink_parameters.start_word << std::endl;
      std::cout << "stop_word     = 0x" << std::hex << slink_parameters.stop_word << std::endl;
    }
 
    //Create the Memory Pool
    m_memoryPool = new MemoryPool_CMEM(10, MAX_PACKET_SIZE_WRDS * sizeof(int));
  
    //Get a page
    try
    {
      mem_page = m_memoryPool->getPage();  //MJ: Check for error!!
    }
    catch (MemoryPoolException& e)
    {
      std::cout << e;
    }
    pci_addr = mem_page->physicalAddress() + sizeof(ROBFragment::ROBHeader);
    virt_addr = (u_long) mem_page->address() + sizeof(ROBFragment::ROBHeader);
    page_size = mem_page->capacity();

    if (verbose) 
    {
      std::cout << "Printing buffer parameters:" << std::endl; 
      std::cout << "Virtual address of buffer  = 0x" << std::hex << virt_addr << std::endl;
      std::cout << "PCI address of data buffer = 0x" << std::hex << pci_addr << std::endl;
      std::cout << "Buffer size in bytes       = " << std::dec << mem_page->capacity() << std::endl;
    }

    // Opens and resets SSP also if LDOWN it tries to reset the SLINK
    code = SLINK_SSPOpen(&slink_parameters, &dev);
    if (code)
    {
      std::cout << "main: SLINK_SSPOpen() FAILED with error " << std::hex << code << std::endl;
      rcc_error_print(stdout, code);
      exit(TmFail);
    }

    if (verbose) 
    {
      std::cout << "SLINK_SSPOpen worked!" << std::endl;
      // Print SLINK status info
      SLINK_PrintState(dev);
    }
    
    // Install signal handler for SIGQUIT & SIGINT
    sigemptyset(&sa.sa_mask);
    sa.sa_flags   = 0;
    sa.sa_handler = sigquit_handler;

    // Dont block in intercept handler
    if (sigaction(SIGQUIT, &sa, NULL) < 0) 
    {
      std::cout << "main: sigaction() FAILED with ";
      code = errno;
      if (code == EFAULT) std::cout << "EFAULT" << std::endl;
      if (code == EINVAL) std::cout << "EINVAL" << std::endl;
      exit(TmUnresolved);
    }

    /* Install signal handler for SIGINT */
    sigemptyset(&saint.sa_mask);
    saint.sa_flags   = 0;
    saint.sa_handler = sigint_handler;

    if (sigaction(SIGINT, &saint, NULL) < 0) 
    {
      std::cout << "main: sigaction() FAILED with ";
      code = errno;
      if (code == EFAULT) std::cout << "EFAULT" << std::endl;
      if (code == EINVAL) std::cout << "EINVAL" << std::endl;
      exit(TmUnresolved);
    }

  #if defined(SLIDAS_PUSH)
    // start run on SLIDAS
    SLIDAS_PushRun(dev);
  #endif
  
    // Open the file for output
    if (writefile)
    {
      outputFile = open(filename, O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
      if (outputFile < 0)
      {
        std::cout << "Failed to open " << filename << std::endl;
        exit(TmUnresolved);
      }
    }
    
    // Open the reference file
    if (checkfile)
    {
      inputFile = open(filename2, O_RDONLY);
      if (inputFile < 0)
      {
        std::cout << "Failed to open " << filename2 << std::endl;
        exit(TmUnresolved);
      }
      
      int refsize = read(inputFile, refbuf, 0x100000);
      std::cout << "The reference file contains " << refsize << " bytes" << std::endl;
      if (refsize < 44)
      {
        std::cout << "The reference file contains less than 44 bytes (i.e. ROD header + trailer)" << std::endl;
        exit(TmUnresolved);
      }
      if (refsize == 0x100000)
      {
        std::cout << "The reference file contains more than 1 MB" << std::endl;
        exit(TmUnresolved);
      }
      close(inputFile);
    }
  
    time(&start_time);
    lpacket = 1;

    std::cout << std::endl << "Press ctrl-\\ to output statistics" << std::endl;
    std::cout << "Press CTRL-C TO quit" << std::endl << std::endl;
  
    // set the buffer address to the proper location
    if (dma == TRUE)
      s_addr = (char *) pci_addr;
    else
      s_addr = (char *) virt_addr;

/* Creation of synchronisation file. After this file is succesfully created, the second test
(source test) is started */
        
    if (DVS)	    
      pmg_initSync();

    ipacket = 1;
    while(ipacket <= ((npackets >= 0) ? npackets : (ipacket))) 
    {
      //Clear the buffer
      int *ptr = (int *)virt_addr;
      for(int loop = 0; loop < MAX_PACKET_SIZE_WRDS; loop++)
        *ptr++ = 0x11223344;

      code = SSP_InitRead(dev, s_addr, page_size, dma, 0); 
      if (code) 
      {
	  std::cout << "main: SSP_InitRead() FAILED with error code " << std::hex << code << std::endl;
	  rcc_error_print(stdout, code);
	  exit(TmFail);
      }
      if(verbose)
      {
        std::cout << "SSP_InitRead called with:" << std::endl;
        std::cout << "s_addr    = 0x" << std::hex << (u_long) s_addr << std::endl;
        std::cout << "page_size = 0x" << std::hex << page_size << std::endl;
        std::cout << "dma       = " << dma << std::endl;
      }

      do 
      {
        code = SSP_ControlAndStatus(dev, &status);
        if (code) 
        {
	  std::cout << "main: SSP_ControlAndStatus() FAILED with " << std::hex << code << std::endl;
	  rcc_error_print(stdout, code);
	  exit(TmFail);
        }
	
        if (status == SLINK_NEED_OF_NEW_PAGE)
        {
	  // We do not expect multi page events
          std::cout << "SSP_ControlAndStatus = SLINK_NEED_OF_NEW_PAGE" << std::endl;
          exit(TmFail);
        }
      } while (status != SLINK_FINISHED);

      packet_size = SSP_PacketSize(dev) >> 2; // We need it in words
      if (!packet_size)
        std::cout << " Empty packet received" << std::endl;
      if (verbose) 
        std::cout << "Packet size received = " << std::dec << packet_size << std::endl;  

      if (print)
      {
        int *ptr;
        ptr = (int *)virt_addr;
        std::cout << "Dumping raw ROD header" << std::endl;
        std::cout << "Word 1 = "<< std::hex << *ptr++ << std::dec << std::endl;
        std::cout << "Word 2 = "<< std::hex << *ptr++ << std::dec << std::endl;
        std::cout << "Word 3 = "<< std::hex << *ptr++ << std::dec << std::endl;
        std::cout << "Word 4 = "<< std::hex << *ptr++ << std::dec << std::endl;
        std::cout << "Word 5 = "<< std::hex << *ptr++ << std::dec << std::endl;
        std::cout << "Word 6 = "<< std::hex << *ptr++ << std::dec << std::endl;
        std::cout << "Word 7 = "<< std::hex << *ptr++ << std::dec << std::endl;
        std::cout << "Word 8 = "<< std::hex << *ptr << std::dec << std::endl;
      }

      // Create a ROB fragment
      robfragment = new ROBFragment(mem_page, packet_size, 0, 0);
                         
      if (print) 
        std::cout << *robfragment;
      
      if (writefile)
      {
        const Buffer* buffer = robfragment->buffer();
        Buffer::page_iterator page = buffer->begin(); 
        if (verbose)
          std::cout << "writing " << packet_size * 4 << " bytes to file" << std::endl; 
	int isok = write(outputFile, (void *)((u_long)(*page)->address() + sizeof(ROBFragment::ROBHeader)), packet_size * 4);
        if (isok < 0)
        {
          std::cout << "Error in writing to file" << std::endl;
          exit(TmUnresolved);
        }
      }
                             
      // Check the event
      if(dcheck)
      {
        flg_error = 0;
        ccode = (CheckType) robfragment->check(ipacket, L1extdcheck);
        if (code && verbose)
          std::cout << "Check of ROD fragment returns error " << ccode << std::endl;
        if (ccode)
          errorFound(ccode);
      }    
      
      if (checkfile)
      {
        unsigned int *refptr = (unsigned int *)refbuf;
        unsigned int *dataptr = (unsigned int *)virt_addr;
        for (int dataword = 0; dataword < packet_size; dataword++)
        {
          if (*refptr != *dataptr)
            std::cout << std::hex << "Data mismatch: Offset = " << dataword * 4 << " Received data = " << *dataptr << "   Expected data = " << *refptr << std::dec << std::endl;
          refptr++;
          dataptr++;
        }
      }
        
      // We don't need the fragment any more
      delete robfragment;
    
      ipacket++;
      
      if (usdelay)
        ts_delay(usdelay);
    }
  }
  catch (std::exception &e) 
  {
    std::cout << "Program ended with an exception: " << std::endl ;
    std::cout << "  " << e.what() ;    
  }
  exit(terminate_it());
}











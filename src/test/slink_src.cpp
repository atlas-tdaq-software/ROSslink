/*****************************************************************************/
/*                                                                           */
/* File Name        : slink_src.c                                            */
/*                                                                           */
/*****************************************************************************/

#include <tmgr/tmresult.h>
#include <cmdl/cmdargs.h>

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include "rcc_error/rcc_error.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSEventFragment/RODFragment.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSslink/slink.h"
#include "DFDebug/DFDebug.h"

using namespace ROS;
using namespace daq::tmgr;

   
// Standard header and trailer sizes
#define HEADER_SIZE_WRDS        (sizeof(RODFragment::RODHeader) / 4)
#define TRAILER_SIZE_WRDS       (sizeof(RODFragment::RODTrailer) / 4)
#define STATUS_SIZE             1
#define STATUS_SIZE_SLIDAS      3

// Minimum packet size is: header + trailer words + at least 4 words
#define MIN_PACKET_SIZE_WRDS    (HEADER_SIZE_WRDS + TRAILER_SIZE_WRDS + 4)  
#define MAX_PACKET_SIZE_WRDS    2048 

#define ROB_POOL_NUMB           10                           //Max. number of ROD fragments
#define ROB_POOL_SIZE           (MAX_PACKET_SIZE_WRDS * 4)   //Max. size of ROD fragments

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif


/* Global variables */
int print = FALSE;
int size = 256;
int maxsize = MAX_PACKET_SIZE_WRDS;
int minsize = MIN_PACKET_SIZE_WRDS;
int npackets = -1;
int occurence = 1;	    
int flg_random = FALSE;
int flg_rodemul = TRUE;
int flg_size = FALSE;
int flg_minsize = FALSE;
int flg_maxsize = FALSE;
int flg_verbose = FALSE;
int first_l1id = 0;	
int d_size;  
int dataSwap = TRUE;
int cwordSwap = TRUE;
int skipevent = 0;
int dblevel = 0;
unsigned int wait = 0 ;
unsigned int module_id = 0;
unsigned int detector_id = 0xa0;
unsigned int l1_type = 0x7 ;
unsigned int det_type = 0xa ; 
SLINK_device *dev;


/*********************/
void terminate_it(void) 
/*********************/
{
  unsigned int code;
    
  std::cout << std::endl << std::endl << "Closing S-link ..." << std::endl;
  
  SLINK_PrintState(dev);
  SPS_Reset(dev);
  
  // Closes the device
  code = SLINK_SPSClose(dev);
  if (code)
    rcc_error_print(stdout, code);
    
  code = SLINK_Close();
  if (code)
    rcc_error_print(stdout, code);
}



/******************************/
void sigquit_handler(int signum)
/******************************/
{
  // ctrl-\ handler
  SLINK_PrintState(dev);
}


/*****************************/
void sigint_handler(int signum)
/*****************************/
{
 // sigint handler
 terminate_it();
 exit(TmPass);
}


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-o x: Occurence                           ->Default: " << occurence << std::endl;
  std::cout << "-S  : SLIDAS emulation (otherwise simulate standard ROD)" << std::endl;
  std::cout << "  Modifiers of -S option:" << std::endl;
  std::cout << "  -O x: Module id (Hex format)            ->Default: 0x" << std::hex << module_id << std::endl;
  std::cout << "  -D x: Detector id (Hex format)          ->Default: 0x" << std::hex << detector_id << std::endl;
  std::cout << "  -L x: L1 Trigger type (Hex format)      ->Default: 0x" << std::hex << l1_type << std::endl;
  std::cout << "  -T x: Detector event type (Hex format)  ->Default: 0x" << std::hex << det_type << std::endl;
  std::cout << "-f x: First L1id for ROD emulation        ->Default: " << std::dec << first_l1id << std::endl;
  std::cout << "-s x: Packet size (incompatible with -r)  ->Default: " << std::dec << size << std::endl;
  std::cout << "-r  : Random packet size (incompatible with -s)" << std::endl;
  std::cout << "  Modifiers of -r option:" << std::endl;
  std::cout << "  -M x: Max packet size                   ->Default: " << std::dec << maxsize << std::endl;
  std::cout << "  -m x: Min packet size                   ->Default: " << std::dec << minsize << std::endl;
  std::cout << "-w x: Wait before sending (in seconds)    ->Default: " << std::dec << wait << std::endl;
  std::cout << "-n x: Number of packets                   ->Default: infinite loop" << std::endl;
  std::cout << "-p  : Print packets" << std::endl;
  std::cout << "-x  : Disable data swap                   ->Default: FALSE" << std::endl;
  std::cout << "-X  : Disable control word swap           ->Default: FALSE" << std::endl;
  std::cout << "-l x: Do not send every x-th event        ->Defaule: 0 (disabled)" << std::endl;
  std::cout << "-e x: Debug level (DFEBUG)                ->Default: 0 (disabled)" << std::endl;
  std::cout << "-v  : Verbose output" << std::endl;
  std::cout << std::endl;
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  ROBFragment *robfragment;
  MemoryPool* rob_pool;
  ReturnCode code;
  SLINK_parameters slink_parameters;
  int bcid = 0, status, ipackets, c;
  unsigned int f_size, d_size, numberOfRODStatusElements;
  unsigned int numberOfDataElements, statusBlockPosition, bunchCrossingId, sourceId, level1Id;
  struct sigaction saint, sa;
  char *pci_addr;
    
  while ((c = getopt(argc, argv,"ho:SO:D:L:T:f:s:rM:m:w:n:pvxXl:e:")) != -1)
    switch (c) 
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
      usage();
      exit(TmUnresolved);
      break;

    case 'o':
      occurence = atoi(optarg);
      if (occurence <= 0 || occurence >= MAX_DEVICES) 
      { 
	std::cout << "Occurence exceeds allowed bounds" << std::endl; 
	exit(TmUnresolved);
      }
      break;
      
    case 'l': skipevent = atoi(optarg);         break; 
    case 'S': flg_rodemul = FALSE;              break;
    case 'O': sscanf(optarg,"%x",&module_id);   break;
    case 'D': sscanf(optarg,"%x",&detector_id); break;
    case 'L': sscanf(optarg,"%x",&l1_type);     break;
    case 'T': sscanf(optarg,"%x",&det_type);    break;
    case 'f': first_l1id = atoi(optarg);        break;
    case 'w': wait = atoi(optarg);              break;
    case 'p': print = TRUE;                     break;
    case 'v': flg_verbose = TRUE;               break;
    case 'x': dataSwap = FALSE;                 break;
    case 'X': cwordSwap = FALSE;                break;
    case 'e': dblevel = atoi(optarg);           break;

    case 's':
      flg_size = TRUE;
      size = atoi(optarg);
      if (size < MIN_PACKET_SIZE_WRDS || size > MAX_PACKET_SIZE_WRDS) 
      {
	std::cout << "Wrong packet size: " << size << std::endl << "(acceptable range: " << MIN_PACKET_SIZE_WRDS 
             << " - " << MAX_PACKET_SIZE_WRDS << ")" << std::endl;
	exit(TmUnresolved) ;
      }
      break;

    case 'r':  
      flg_random = TRUE;
      //init random generation
      srand((unsigned int)time(NULL));
      break;

    case 'M':
      flg_maxsize = TRUE;
      maxsize = atoi(optarg);
      if (maxsize > MAX_PACKET_SIZE_WRDS) 
      {
	std::cout << "Wrong max packet size: " << size << std::endl 
             << "(must be < "  << MAX_PACKET_SIZE_WRDS << ")" << std::endl;
	exit(TmUnresolved) ;
      }
      break;
 
    case 'm':
      flg_minsize = TRUE;
      minsize = atoi(optarg);
      if (minsize < MIN_PACKET_SIZE_WRDS) 
      {
	std::cout << "Wrong min packet size: " << size << std::endl
             << "(must be > " << MIN_PACKET_SIZE_WRDS << ")" << std::endl;
	exit(TmUnresolved) ;
      }
      break;
 
    case 'n':
      npackets = atoi(optarg);
      if (npackets<0) 
      {
	std::cout << "number of packets must be positive" << std::endl;
	exit(TmUnresolved);
      } 
      break;

    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << "[options]" << std::endl;
      usage();
      exit (TmUnresolved);
    }

  // Check compatibility of options
  if (flg_random && flg_size) 
  { 
    std::cout << "Options -s and -r are incompatible" << std::endl;
    exit (TmUnresolved);
  }
  
  if ( (flg_minsize || flg_maxsize) && !flg_random) 
  {
    std::cout << "-m and -M options can only be used together with -r option" << std::endl;
    exit (TmUnresolved);
  }
 
  DF::GlobalDebugSettings::setup(dblevel, DFDB_SLINK);

  //wait some time to be sure that slink_dst has started
  sleep(wait);

  code = SLINK_Open();
  if (code) 
  {
    rcc_error_print(stdout, code);
    exit (TmFail);
  }

  // Set parameters to default values
  SLINK_InitParameters(&slink_parameters);

  // Now override some parameters
  slink_parameters.occurence = occurence;
  
  if (dataSwap == TRUE)
     slink_parameters.byte_swapping = SLINK_ENABLE;
  else
     slink_parameters.byte_swapping = SLINK_DISABLE;

  if (cwordSwap == TRUE)
  {
    slink_parameters.start_word = 0x0000F0B0;
    slink_parameters.stop_word  = 0x0000F0E0;
  }
  else
  {
    slink_parameters.start_word = 0xB0F00000;
    slink_parameters.stop_word  = 0xE0F00000;
  }

  if (flg_verbose) 
  {
    std::cout << "SLINK Parameters : " << std::dec << std::endl;
    std::cout << "data width       : " << std::dec << slink_parameters.data_width << std::endl;
    std::cout << "occurence        : " << std::dec << slink_parameters.occurence << std::endl;
    std::cout << "timeout          : " << std::dec << slink_parameters.timeout << std::endl;
    std::cout << "byte swapping    : " << std::dec << slink_parameters.byte_swapping << std::endl; 
    std::cout << "start word       : " << std::hex << slink_parameters.start_word << std::endl;
    std::cout << "stop word        : " << std::hex << slink_parameters.stop_word << std::endl;
  }

  // Open the device
  code = SLINK_SPSOpen(&slink_parameters, &dev);
  if (code)
  {
    rcc_error_print(stdout, code);
    exit (TmFail);
  }

  if (flg_verbose)
    SLINK_PrintState(dev);

  // Install signal handler for SIGQUIT
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit (TmUnresolved);
  }

  // Install signal handler for SIGINT
  sigemptyset(&saint.sa_mask);
  saint.sa_flags   = 0;
  saint.sa_handler = sigint_handler;

  if (sigaction(SIGINT, &saint, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TmUnresolved);
  }

  std::cout << std::endl <<"Press ctrl-\\ to output statistics" << std::endl;
  std::cout << "Press ctrl-c to quit" << std::endl << std::endl;

  //Create a memory pool
  if (flg_verbose) 
  {
    std::cout << "Creating MemoryPool_CMEM" << std::endl;
    std::cout << "Number of pages = " << ROB_POOL_NUMB << std::endl;
    std::cout << "Page size = " << std::hex << ROB_POOL_SIZE << " bytes" << std::endl;
  }

  try
  {
    rob_pool = new MemoryPool_CMEM(ROB_POOL_NUMB, ROB_POOL_SIZE);
  }
  catch (MemoryPoolException& e)
  {
    std::cout << e << std::endl;
    exit(TmUnresolved);
  }

  // Event loop
  for (ipackets = 1; npackets == -1 || ipackets <= npackets ; ipackets++) 
  {    
    // Generate the L1ID  
    if (!flg_rodemul) // wrap a la SLIDAS
      level1Id = (ipackets + first_l1id - 1) & 0xffff;
    else 
      level1Id = (ipackets + first_l1id - 1);
    
    // Generate the Source ID
    sourceId = (module_id & 0xff) | ((detector_id & 0xff) << 8);  //module type = 0 (ROD)
    
    // Generate the BCID a la SLIDAS ... yes I know its wrong  
    if (ipackets == 1) 
      bunchCrossingId = bcid = 0;
    else 
      bunchCrossingId = (bcid += 3) & 0xfff;

    if (!flg_rodemul)
      numberOfRODStatusElements = STATUS_SIZE_SLIDAS;
    else
      numberOfRODStatusElements = STATUS_SIZE;

    // Generate the data size
    if (flg_random) 
    {
      d_size = rand() % (maxsize - minsize + 1);  // generate size    
      if (flg_verbose)
        std::cout << "Random fragment size = " << d_size << std::endl;
    }
    else
    {
      d_size = size - HEADER_SIZE_WRDS - TRAILER_SIZE_WRDS - numberOfRODStatusElements;
      if (flg_verbose)
        std::cout << "fixed fragment size = " << d_size << std::endl;
    }
         
    numberOfDataElements = d_size;
    statusBlockPosition = 0;
    
    // Create an emulated ROB fragment
    // I know that we need a ROD fragment but the ROSEventFragment package
    // should not have to deal with ROD fragments
    try
    {
    robfragment = new ROBFragment(rob_pool, sourceId, level1Id,  bunchCrossingId, l1_type, 
                                  det_type,  numberOfRODStatusElements, numberOfDataElements,
                                  statusBlockPosition);
    }
    catch (ROSException& e)
    {
      std::cout << "ROSException:" << std::endl;
      std::cout << e << std::endl;
    }
    catch(...)
    {
      std::cout << "Exception received from <new ROBFragment>" << std::endl;
      exit(TmUnresolved);
    }
    
    // Fill the data block
    robfragment->fill_data();  
         
    // Make some modifications to the ROD fragment
    if (!flg_rodemul) 
      robfragment->emulate_SLIDAS();    
    
    // Get the PCI address of the ROD fragment inside the ROB fragment
    pci_addr = (char *)robfragment->getPCIaddress() + sizeof(ROBFragment::ROBHeader); 
    
    // Print packet
    if (print)
    {
      std::cout << "Printing packet" << std::endl;
      std::cout << *robfragment;
      std::cout << std::endl;
    }

    if ((skipevent == 0) || (level1Id % skipevent))
    {
      //Write packet to slink
      f_size = 4 * (d_size + HEADER_SIZE_WRDS + TRAILER_SIZE_WRDS + numberOfRODStatusElements);
      if (flg_verbose)
          std::cout << "Sending packet with " << std::dec << (f_size >> 2) << " words" << std::endl;
      code = SPS_InitWrite(dev, pci_addr, f_size, SLINK_BOTH_CONTROL_WORDS, SLINK_ENABLE);
      if (code) 
      {
        rcc_error_print(stdout, code);
        exit(TmFail);
      }

      //Wait until packet has been sent
      do 
      {
        code = SPS_ControlAndStatus(dev, &status);
        if (code) 
        {
          rcc_error_print(stdout, code);
	  exit(TmFail);
        }
      } while (status != SLINK_FINISHED);
    }
        
    //Remove the ROB Fragment
    delete robfragment;
    
  }
  terminate_it();
  exit(TmPass);
}


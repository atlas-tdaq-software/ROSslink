
/*****************************************************************************/
/*                                                                           */
/* File Name        : slink.c                                                */
/* Version          : 2.0                                                    */
/* Author           : F.Pennerath, D.Francis, M.Niculescu, J.Petersen, M.Joos*/
/************ C 2002 -  United Artists (and hackers) *************************/

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include "rcc_error/rcc_error.h"
#include "io_rcc/io_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSslink/s5933.h"
#include "ROSslink/slink.h"

#define FIFO_WAIT

/****************************************************************************/
/*                   Constant to identify the S-LINK structure              */
/****************************************************************************/
#define SPS_STRUCTURE_ID 0x15482660
#define SSP_STRUCTURE_ID 0x83347100

/****************************************************************************/
/*                     PCI VID and DID words to identify                    */
/*                        the SPS and SSP interfaces                        */
/****************************************************************************/
#define SPS_VENDOR       0x10dc
#define SPS_DEVICE       0x0010
#define SLINK_SPS_ID     ((SPS_DEVICE << 16) | SPS_VENDOR)

#define SSP_VENDOR       0x10dc
#define SSP_DEVICE       0x0011
#define SLINK_SSP_ID     ((SSP_DEVICE << 16) | SSP_VENDOR)

/****************************************************************************/
/*             Maximum timeout for reset operation in milliseconds          */
/****************************************************************************/
#define SLINK_RESET_TIMEOUT 1000

/****************************************************************************/
/*          Macro waiting one micro : be careful : system dependent         */
/****************************************************************************/
#define SLINK_Sleep(t) usleep((t))

/****************************************************************************/
/*                         Declaration of macros                            */
/****************************************************************************/
#define SPS_bad_structure(device) ((device)->struct_id != SPS_STRUCTURE_ID)
#define SSP_bad_structure(device) ((device)->struct_id != SSP_STRUCTURE_ID)

#define OPEN_STATE if (!package_open) return RCC_ERROR_RETURN(0, SLINK_NOTOPEN)

#define VALID_DEVICE \
      if (device == (SLINK_device *) -1) \
        return RCC_ERROR_RETURN(0, SLINK_DEVICE_NOT_FOUND) 

#define VALID_OCCURENCE(occurence) \
      if ((occurence < 1) || (occurence >= MAX_DEVICES)) \
        return RCC_ERROR_RETURN(0, SLINK_BADOCC) 

#define MAX_DEVICES_OPEN \
      if (devices_open >= MAX_DEVICES) \
        return RCC_ERROR_RETURN(0, SLINK_MAXDEVICES)

#define show(x)  printf x

/****************************************************************************/
/*                         Error descriptions                               */
/****************************************************************************/
#define SLINK_OK_STR                    "No error."
#define SLINK_ISOPEN_STR                "Already opened."
#define SLINK_NOTOPEN_STR               "No open performed."
#define SLINK_DEVICEOPEN_STR            "Device already opened."
#define SLINK_MAXDEVICES_STR            "Maximum number of devices opened."
#define SLINK_NOTMYFAULT_STR            "Failed due to an error in another package."
#define SLINK_DEVICE_NOT_FOUND_STR      "Requested device was not found."
#define SLINK_FORBIDDEN_ACCESS_STR      "Cannot access to the device."
#define SLINK_NOT_ENOUGH_MEMORY_STR     "Not enough memory to allocate device resources."
#define SLINK_BAD_POINTER_STR           "Invalid device pointer."
#define SLINK_BAD_DATA_WIDTH_STR        "Data width provided in parameter has a wrong value."
#define SLINK_BAD_PARAMETERS_STR        "Parameter provided to function has a wrong value."
#define SLINK_DEVICE_DOWN_STR           "The device seems to be down (LDOWN#=0)."
#define SLINK_FIFO_FULL_STR             "The fifo is full."
#define SLINK_FIFO_EMPTY_STR            "The fifo is empty."
#define SLINK_DETACH_FAILED_STR         "Unable to remove the device mapping ressources."
#define SLINK_TRANSFER_NOT_FINISHED_STR "A transfer is currently not finished."
#define SLINK_TRANSFER_NOT_STARTED_STR  "There is no transfer currently running."
#define SLINK_DOWN_LINK_STR             "The reset failed. The link cannot be reset."
#define SLINK_LOST_IN_FUNCTION_STR      "Invalid Program counter."
#define SLINK_BAD_STATUS_STR            "Value of transfer status unknown or un-appropriate."
#define SLINK_UNEXPECTED_CW_STR         "Unexpected Control Word."
#define SLINK_BADOCC_STR                "Invalid occurence number."
#define SLINK_UNKNOWN_STR               "Un-anticipated error."
#define SLINK_NOCODE_STR                "No such error code."


#ifndef _1KBYTE
#define _1KBYTE                 0x1000
#endif

#ifndef TRUE
#define TRUE                    0x01
#endif

#ifndef FALSE
#define FALSE                   0x00
#endif


#define SPS_OUT_REG S5933_OMB1
#define SPS_IN_REG  S5933_IMB4
#define SSP_OUT_REG S5933_OMB1
#define SSP_IN_REG  S5933_IMB4

#define SLINK_WRONG_WORD   0
#define SLINK_RIGHT_WORD   1

#define SLINK_CONTROL_WORD 0
#define SLINK_DATA_WORD    1

/* Bit field definitions */
const unsigned int SPS_LDOWN    = 0x10000000;
const unsigned int SPS_LRLS     = 0x0f000000;
const unsigned int SPS_LRL3     = 0x08000000;
const unsigned int SPS_LRL2     = 0x04000000;
const unsigned int SPS_LRL1     = 0x02000000;
const unsigned int SPS_LRL0     = 0x01000000;

const unsigned int SPS_UDWS     = 0x00000018;
const unsigned int SPS_UDW1     = 0x00000010;
const unsigned int SPS_UDW0     = 0x00000008;
const unsigned int SPS_URESET   = 0x00000004;
const unsigned int SPS_UTEST    = 0x00000002;
const unsigned int SPS_UCTRL    = 0x00000001;

const unsigned int SSP_OVFLW    = 0x08000000;
const unsigned int SSP_LDOWN    = 0x04000000;
const unsigned int SSP_LCTRL    = 0x02000000;
const unsigned int SSP_LDERR    = 0x01000000;
const unsigned int SSP_LINES    = 0x03000000;

const unsigned int SSP_EXPLINES = 0x03000000;
const unsigned int SSP_EXPLCTRL = 0x02000000;
const unsigned int SSP_EXPLDERR = 0x01000000;
const unsigned int SSP_URLS     = 0x00000f00;
const unsigned int SSP_URL3     = 0x00000800;
const unsigned int SSP_URL2     = 0x00000400;
const unsigned int SSP_URL1     = 0x00000200;
const unsigned int SSP_URL0     = 0x00000100;
const unsigned int SSP_UDWS     = 0x00000018;
const unsigned int SSP_UDW1     = 0x00000010;
const unsigned int SSP_UDW0     = 0x00000008;
const unsigned int SSP_URESET   = 0x00000004;
const unsigned int SSP_UTDO     = 0x00000002;
const unsigned int SSP_FRESET   = 0x00000001;

const          int SPS_UCTRL_BPOS = 0;
const          int SPS_UDWS_BPOS  = 3;

const          int SSP_LCTRL_BPOS = 25;
const          int SSP_UDWS_BPOS  = 3;

/****************************************************************************/
/*                           Global variables                               */
/****************************************************************************/
int devices_open;
int package_open = 0x0;

struct device_info {
  int   used;
  char  name[11];
  char *vadd;
} sps_devices[MAX_DEVICES], ssp_devices[MAX_DEVICES];

/* Disable/enable strings */
static char * SLINK_enable_messages[] = {"disabled", "enabled"};

/* Packet format strings */
static char * SLINK_format_messages[] = {"No control words",
					 "Start control words",
					 "Stop control words",
					 "Both start and stop control words"};

/* Status description strings */
static char * SLINK_status_messages[] = {
  "Free. No request of transfer.",
  "Transfer initiated. Waiting for a packet.",
  "Transfer aborted. Dump of the last defective packet.",
  "Transfer processed. Receiving a packet.",
  "Transfer processed. Sending a packet.",
  "Transfer paused. Waiting for a new buffer.",
  "Transfer finished."
};


/**********************************************************************************************/
static void WriteReg(volatile unsigned int *base, unsigned int const offset, unsigned int value)
/**********************************************************************************************/
{
  volatile unsigned int *ptr = base + (offset >> 2);

  *ptr = BSWAP(value); 
}

/*********************************************************************************/
static unsigned int ReadReg(volatile unsigned int *base, unsigned int const offset)
/*********************************************************************************/
{
  return BSWAP(*(base + (offset >> 2)));
}


/*********************************************************************/
ReturnCode SLINK_err_get(err_pack err, err_str Pid_str, err_str en_str)
/*********************************************************************/
{
  strcpy(Pid_str, P_ID_SLINK_STR) ;
 
  switch (err) {
  case SLINK_OK:                    strcpy(en_str, SLINK_OK_STR)                    ; break;
  case SLINK_ISOPEN:                strcpy(en_str, SLINK_ISOPEN_STR)                ; break;
  case SLINK_NOTOPEN:               strcpy(en_str, SLINK_NOTOPEN_STR)               ; break;
  case SLINK_DEVICEOPEN:            strcpy(en_str, SLINK_DEVICEOPEN_STR)            ; break;
  case SLINK_MAXDEVICES:            strcpy(en_str, SLINK_MAXDEVICES_STR)            ; break;
  case SLINK_NOTMYFAULT:            strcpy(en_str, SLINK_NOTMYFAULT_STR)            ; break;
  case SLINK_DEVICE_NOT_FOUND:      strcpy(en_str, SLINK_DEVICE_NOT_FOUND_STR)      ; break;
  case SLINK_FORBIDDEN_ACCESS:      strcpy(en_str, SLINK_FORBIDDEN_ACCESS_STR)      ; break;
  case SLINK_NOT_ENOUGH_MEMORY:     strcpy(en_str, SLINK_NOT_ENOUGH_MEMORY_STR)     ; break;
  case SLINK_BAD_POINTER:           strcpy(en_str, SLINK_BAD_POINTER_STR)           ; break;
  case SLINK_BAD_DATA_WIDTH:        strcpy(en_str, SLINK_BAD_DATA_WIDTH_STR)        ; break;
  case SLINK_BAD_PARAMETERS:        strcpy(en_str, SLINK_BAD_PARAMETERS_STR)        ; break;
  case SLINK_DEVICE_DOWN:           strcpy(en_str, SLINK_DEVICE_DOWN_STR)           ; break;
  case SLINK_FIFO_FULL:             strcpy(en_str, SLINK_FIFO_FULL_STR)             ; break;
  case SLINK_FIFO_EMPTY:            strcpy(en_str, SLINK_FIFO_EMPTY_STR)            ; break;
  case SLINK_DETACH_FAILED:         strcpy(en_str, SLINK_DETACH_FAILED_STR)         ; break;
  case SLINK_TRANSFER_NOT_FINISHED: strcpy(en_str, SLINK_TRANSFER_NOT_FINISHED_STR) ; break;
  case SLINK_TRANSFER_NOT_STARTED:  strcpy(en_str, SLINK_TRANSFER_NOT_STARTED_STR)  ; break;
  case SLINK_DOWN_LINK:             strcpy(en_str, SLINK_DOWN_LINK_STR)             ; break;
  case SLINK_LOST_IN_FUNCTION:      strcpy(en_str, SLINK_LOST_IN_FUNCTION_STR)      ; break;
  case SLINK_BAD_STATUS:            strcpy(en_str, SLINK_BAD_STATUS_STR)            ; break;
  case SLINK_UNEXPECTED_CW:         strcpy(en_str, SLINK_UNEXPECTED_CW_STR)         ; break;
  case SLINK_BADOCC:                strcpy(en_str, SLINK_BADOCC_STR)                ; break;
  case SLINK_UNKNOWN:               strcpy(en_str, SLINK_UNKNOWN_STR)               ; break;
  case SLINK_NOCODE:                strcpy(en_str, SLINK_NOCODE_STR)                ; break;
  default:
    strcpy(en_str, SLINK_NOCODE_STR);

    return RCC_ERROR_RETURN(0, SLINK_NOCODE);
    break; 
  }
 
  return RCC_ERROR_RETURN(0, SLINK_OK);
}


/*********************************************************/
ReturnCode SLINK_PrintErrorMessage(unsigned int error_code)
/*********************************************************/
{
  err_str package, comment;

  SLINK_err_get(error_code, package, comment);
  printf("%s\n", comment);
  return RCC_ERROR_RETURN(0, SLINK_OK);
}


/****************************************************************************/
/* Name           : SLINK_GetEnablesMessage                                 */
/* Description    : Returns a string of character describing                */
/*                  a enable/disable flag                                   */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* enable_flag         | int                | Enable flag to describe       */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : A pointer on characters, pointing on the enable flag    */
/*                  description.                                            */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
char *SLINK_GetEnableMessage(int enable_flag)
{
  DEBUG_TEXT(DFDB_SLINK, 15, "SLINK_GetEnableMessage: called");
  if ((enable_flag < SLINK_DISABLE) || (enable_flag > SLINK_ENABLE)) { 
    return "Unknown enable flag value.";
  }

  return SLINK_enable_messages[enable_flag];
}

/****************************************************************************/
/* Name           : SLINK_GetFormatMessage                                  */
/* Description    : Returns a string of character describing a packet format*/
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* format_code         | int                | Contains a integer code       */
/*                     |                    | describing a packet format.   */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : A pointer on characters, pointing on the format         */
/*                  description.                                            */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
char * SLINK_GetFormatMessage(int format_code)
{
  DEBUG_TEXT(DFDB_SLINK, 15, "SLINK_GetFormatMessage: called");
  if ((format_code < SLINK_NO_CONTROL_WORD) ||
      (format_code > SLINK_BOTH_CONTROL_WORDS)) {
    return "Unknown packet format.";
  }

  return SLINK_format_messages[format_code];
}

/****************************************************************************/
/* Name           : SLINK_GetStatusMessage                                  */
/* Description    : Returns a string of character describing a status       */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* status_code         | int                | Contains a integer code       */
/*                     |                    | describing a S-LINK status.   */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : A pointer on characters, pointing on the status         */
/*                  description.                                            */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
char * SLINK_GetStatusMessage(int status_code)
{
  DEBUG_TEXT(DFDB_SLINK, 15, "SLINK_GetStatusMessage: called");
  if ((status_code < 0) || (status_code >= 
			    SLINK_INSERT_NEW_STATUS_CODES_BEFORE_THIS_ONE)) {
    return "Unknown status code.";
  }

  return SLINK_status_messages[status_code];
}

/****************************************************************************/
/* Name           : SLINK_PrintState                                        */
/* Description    : Displays the current state of the device                */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : An error code                                           */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
ReturnCode SLINK_PrintState(SLINK_device *device)
{
  OPEN_STATE;

  DEBUG_TEXT(DFDB_SLINK, 15, "SLINK_PrintState: called");
  VALID_OCCURENCE(device->params.occurence);

  printf("S-LINK library reports:\n");

  /* Identification part */
  printf("  Device identification :\n");
  printf("    Type     : ");

  if (device->struct_id == SPS_STRUCTURE_ID)
    printf("SPS (PCI to S-LINK)\n");
  else if (device->struct_id == SSP_STRUCTURE_ID)
    printf("SSP (S-LINK to PCI)\n");
  else {
    printf("Unknown !\n");
    return RCC_ERROR_RETURN(0, SLINK_BAD_POINTER);
  }

  printf("    Occurence: %d\n", device->params.occurence);

  /* Transfer parameter part */
  printf("  Device transfer parameters :\n");
  printf("    Current status : %s\n",
	  SLINK_GetStatusMessage(device->transfer_status));
  printf("    DMA %s.\n", SLINK_enable_messages[device->dma]);
  printf("    Byte swapping %s.\n",
	SLINK_GetEnableMessage(device->params.byte_swapping));
  printf("    Start control word = 0X%8X\n",
	  (unsigned int) device->params.start_word);
  printf("    Stop control word  = 0X%8X\n",
	  (unsigned int) device->params.stop_word);
  printf("    Timeout            = %d\n", device->params.timeout);

  printf("    Data width         = ");
  switch (device->params.data_width) {
  case SLINK_8BITS:
    printf("8 bits\n");
    break;

  case SLINK_16BITS:
    printf("16 bits\n");
    break;

  case SLINK_32BITS:
    printf("32 bits\n");
    break;

  default:
    printf("Unknown\n");
    break;
  }

  /* Specific device part */
  printf("  Device specific fields :\n");

  switch (device->struct_id) {
  case SPS_STRUCTURE_ID:
    printf("    Packet format      = %s.\n",
	    SLINK_GetFormatMessage((int) device->specific.sps.packet_format));
    break;

  case SSP_STRUCTURE_ID:
    printf("    Multiple pages transfer %s.\n",
	SLINK_GetEnableMessage(device->specific.ssp.multiple_pages_transfer));
    printf("    Keep bad packets %s.\n",
	  SLINK_GetEnableMessage( device->params.keep_bad_packets));
    break;

  default:
    break;
  }

  /* Statistics part */
  printf("  Device statistics:\n");

  switch (device->struct_id) {
  case SPS_STRUCTURE_ID:
    printf("    %d correctly sent packets.\n", device->stats.good_packets);
    break;

  case SSP_STRUCTURE_ID:
    printf("    %d correct received packets.\n", device->stats.good_packets);
    printf("    %d transmission errors.\n", device->stats.transmission_errors);
    printf("    %d losts of start control word.\n", device->stats.lost_starts);
    printf("    %d losts of end control word.\n", device->stats.lost_ends);
    printf("    %d unknown received control words.\n", device->stats.unknown_control_words);
    printf("    %d overflown buffers.\n", device->stats.overflown_buffers);
    printf("    %d asks for a new page.\n", device->stats.asks_for_new_page);
    break;

  default :
    break;
  }

  return RCC_ERROR_RETURN(0, SLINK_OK);
}

/****************************************************************************/
/* Name           : SLINK_WhichTypeOfDevice                                 */
/* Description    : Returns the type of the device                          */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : A S-LINK device type  ( SLINK_device_type)              */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
SLINK_device_type SLINK_WhichTypeOfDevice(SLINK_device *device)
{
  DEBUG_TEXT(DFDB_SLINK, 15, "SLINK_WhichTypeOfDevice: called");
  switch (device->struct_id) {
  case SPS_STRUCTURE_ID:
    return SPS;
    break;
  case SSP_STRUCTURE_ID:
    return SSP;
    break;
  default:
    return UNKNOWN;
    break;
  }
}


/****************************************/
static void *uio_malloc(unsigned int size)
/****************************************/
{
  void *mem;

  DEBUG_TEXT(DFDB_SLINK, 15, "uio_malloc: called");

  mem = malloc((size_t) size);
  memset(mem, 0x0, (size_t) size);

  return mem; 
}


/**********************************************************************************/
static ReturnCode PCI_Stuff(unsigned int vendor, unsigned int device, int occurence, 
                            unsigned long *virt, unsigned int *dev_handle)
/**********************************************************************************/
{
  ReturnCode r_code;
  unsigned int cmd, pci, handle;
  
  DEBUG_TEXT(DFDB_SLINK, 15, "PCI_Stuff: called");
  r_code = IO_PCIDeviceLink(vendor, device, occurence, &handle);
  if (r_code)
    return RCC_ERROR_RETURN(r_code, SLINK_NOTMYFAULT);
    
  r_code = IO_PCIConfigReadUInt(handle, PCI_COMMAND, &cmd);
  if (r_code)
    return RCC_ERROR_RETURN(r_code, SLINK_NOTMYFAULT);
  cmd |= PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER | PCI_COMMAND_PARITY | PCI_COMMAND_SERR;
  
  r_code = IO_PCIConfigWriteUInt(handle, PCI_COMMAND, cmd);
  if (r_code)
    return RCC_ERROR_RETURN(r_code, SLINK_NOTMYFAULT);

  r_code = IO_PCIConfigReadUInt(handle, PCI_BASE_ADDRESS_0, &pci);
  if (r_code)
    return RCC_ERROR_RETURN(r_code, SLINK_NOTMYFAULT); 
  
  /*
   * ATTENTION: this is hardwired for powerpc ... needs to be cleaned up
   * assumes an s5933 PCI controller
   * ie ALWAYS mapped into mem space on pci bus
   */
#if defined(__PPC__) || defined(__powerpc__)
  pci = pci | 0xc0000000;
#endif
  
  r_code = IO_PCIMemMap(pci, _1KBYTE, virt);
  if (r_code)
    return RCC_ERROR_RETURN(r_code, SLINK_NOTMYFAULT);
 
  *dev_handle = handle;

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}


/*************************/
ReturnCode SLINK_Open(void)
/*************************/
{
  ReturnCode r_code;
  int i;

  DEBUG_TEXT(DFDB_SLINK, 15, "SLINK_Open: called");
  if (package_open++) return RCC_ERROR_RETURN(0, SLINK_ISOPEN);

  /* Open the error package */ 
  if ((r_code = rcc_error_init(P_ID_SLINK, SLINK_err_get))) 
  {
    package_open--; 
    return RCC_ERROR_RETURN(r_code, SLINK_NOTMYFAULT);
  }

  /* Access to the PCI library */
  r_code = IO_Open();    
  if (r_code) 
  { 
    package_open--; 
    return RCC_ERROR_RETURN(r_code, SLINK_NOTMYFAULT);
  }

  for (i=0; i<MAX_DEVICES; i++) {
    ssp_devices[i].used = FALSE;
    sprintf(ssp_devices[i].name, "SSP_S5933_%d", i);
    ssp_devices[i].vadd = (char *) NULL;

    sps_devices[i].used = FALSE;
    sprintf(sps_devices[i].name, "SPS_S5933_%d", i);
    sps_devices[i].vadd = (char *) NULL;
  }

  devices_open = 0x0;
  
  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/**************************/
ReturnCode SLINK_Close(void)
/**************************/
{
  ReturnCode r_code;

  DEBUG_TEXT(DFDB_SLINK, 15, "SLINK_Close: called");
  OPEN_STATE;

  if (!--package_open) {

    devices_open = 0;

    if ((r_code = IO_Close()))
      show(("SLINK_Close: IO_Close() FAILED [%#x]\n", r_code));
  }
      
  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}


/****************************************************************************/
/* Name           : LSC_Reset                                               */
/* Description    : Resets the LSC                                          */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : An error code                                           */
/*                                                                          */
/* Notes          : 970828 Michaela                                         */
/*                  reset the LSC					    */
/****************************************************************************/
static ReturnCode LSC_Reset(SLINK_device *device)
{
  static volatile s5933_regs *regs;
  int timeout;

  DEBUG_TEXT(DFDB_SLINK, 15, "LSC_Reset: called");
  OPEN_STATE;

  VALID_OCCURENCE(device->params.occurence);

  /* Checks if the device pointer is right */
  if (SPS_bad_structure(device) == TRUE) {
    return SLINK_BAD_POINTER;
  }

  /* Gets the address of the registers */
  regs = device->regs;

  /* Resets the link */
  device->specific.sps.out &= ~SPS_URESET; 
  WriteReg(regs, SPS_OUT_REG, device->specific.sps.out);

  /* Waits few microseconds */
  SLINK_Sleep(10);

  /* Waits LDOWN# is removed unless the timeout expired */
  timeout = 0;

  do {
    device->specific.sps.in = ReadReg(regs, SPS_IN_REG);

    /* Waits a milli second */
    SLINK_Sleep(1000);

    timeout++;

    /* Returns an error if the timeout is expired */
    if (timeout > SLINK_RESET_TIMEOUT)
      return RCC_ERROR_RETURN(0x00, SLINK_DOWN_LINK);

  } while ((device->specific.sps.in & SPS_LDOWN) == 0);

  /* Removes the reset of the link */
  device->specific.sps.out |= SPS_URESET;

  WriteReg(regs, SPS_OUT_REG, device->specific.sps.out);

  /* Does the software reset */
  device->transfer_status = SLINK_FREE;

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************************/
/* Name           : SPS_Reset                                               */
/* Description    : Resets a SPS interface                                  */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : A error message                                         */
/*                                                                          */
/* Notes          : 970828 Michaela                                         */
/*                  Reset only the SPS interface                            */
/****************************************************************************/
ReturnCode SPS_Reset(SLINK_device *device)
{
  static volatile s5933_regs *regs;

  DEBUG_TEXT(DFDB_SLINK, 15, "SPS_Reset: called");
  OPEN_STATE;

  VALID_OCCURENCE(device->params.occurence);

  /* Checks if the device pointer is right */
  if (SPS_bad_structure( device) == TRUE)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);

  /* Gets the address of the registers */
  regs = device->regs;

  /* Resets the S5933's fifo */
  WriteReg(regs, S5933_MCSR, (ReadReg(regs, S5933_MCSR) | S5933_MCSR_P2AFR));
  
  /* Resets the add-on card */
  WriteReg(regs, S5933_MCSR, (ReadReg(regs, S5933_MCSR) | S5933_MCSR_AOR));

  /* Waits few microseconds */
  SLINK_Sleep(100);

  /* Removes the reset signal on the add-on card */
  WriteReg(regs, S5933_MCSR, (ReadReg(regs, S5933_MCSR) & ~S5933_MCSR_AOR));

  SLINK_Sleep(10);

  device->transfer_status = SLINK_FREE;

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}


/******************************** *******************************************/
/* Name           : SLINK_SPSOpen                                           */
/* Description    : Gives access to a SPS interface and initializes it.     */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* params              | SLINK_parameters*  | Includes the initializing     */
/*                     |                    | parameters.                   */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device **    | Contains the address of a     */
/*                     |                    | structure defining the chip   */
/*                     |                    | when the function returned    */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : An error code                                           */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
ReturnCode SLINK_SPSOpen(SLINK_parameters *params, SLINK_device **device)
{
  ReturnCode r_code;
  unsigned long virt;
  register volatile s5933_regs *regs;

  DEBUG_TEXT(DFDB_SLINK, 15, "SLINK_SPSOpen: called");
  OPEN_STATE;

  VALID_OCCURENCE(params->occurence);

  MAX_DEVICES_OPEN;

  *device = (SLINK_device *) -1;

  /* Only carry on if this device not already allocated */
  if (sps_devices[params->occurence].used)
    return RCC_ERROR_RETURN(0, SLINK_DEVICEOPEN);
  
  r_code = PCI_Stuff(SPS_VENDOR, SPS_DEVICE, params->occurence, &virt, &params->handle);
  if (r_code)
    return r_code;
    
  sps_devices[params->occurence].vadd = (char *)virt;

  regs = (s5933_regs *)virt;

  /* Allocates memory for the device structure */
  if ((*device = (SLINK_device *) uio_malloc(sizeof(SLINK_device))) == NULL) 
  {
    int occ = params->occurence;     
     
    r_code = IO_PCIMemUnmap(virt, _1KBYTE);
    if (r_code)
      show(("SLINK_SPSOpen: IO_PCIMemUnmap() FAILED [%#x] occ. %d\n", r_code, occ));

    r_code = IO_PCIDeviceUnlink(params->handle);
    if (r_code)
      show(("SLINK_SPSOpen: IO_PCIDeviceUnlink() FAILED [%#x] occ. %d\n", r_code, occ));
          
    r_code = IO_Close();
    if (r_code)
      show(("SLINK_SPSOpen: IO_Close() FAILED [%#x] occ. %d\n", r_code, occ));

    return RCC_ERROR_RETURN(0x00, SLINK_NOT_ENOUGH_MEMORY);
  }

  bcopy((char *) params, (char *) &((*device)->params), sizeof(SLINK_parameters));

  bzero((char *) &((*device)->stats), sizeof(SLINK_statistics));

  /* Be sure reserved bits in specified control words are 0 */
  (*device)->params.start_word &= ~0xf;
  (*device)->params.stop_word &= ~0xf;

  /* Initializes address parameters */
  (*device)->regs = regs;

  /* Initializes transfer parameters */
  (*device)->transfer_status = SLINK_FREE;

  (*device)->error_messages = SLINK_NO_ERROR;

  (*device)->initial_size = 0x00;

  (*device)->dma = SLINK_ENABLE;

  /* Initializes the output SLINK register */
  (*device)->specific.sps.out = 0x00;

  /* Sets the data width */
  (*device)->specific.sps.out &= ~SPS_UDWS;
  (*device)->specific.sps.out |= ((*device)->params.data_width & 0x3) << SPS_UDWS_BPOS;

  /* Set the type of word to CONTROL */
  (*device)->specific.sps.out |= SLINK_CONTROL_WORD;

  /* Sets the URESET# and UTEST# signals */
  (*device)->specific.sps.out |= SPS_URESET;

  (*device)->specific.sps.out |= SPS_UTEST;

  /* Initialises the MCSR register */
  (*device)->mcsr = 0x00;

#if defined(REFRAINED_REQUEST)
  /* DMA asks for bus mastering only if 4 or more free rooms in S5933 fifo */
  (*device)->mcsr |= S5933_MCSR_RMS;
#endif

  /* Resets the DMA counter */
  WriteReg(regs, S5933_MRTC, 0x00);

  /* Enables the DMA */
  (*device)->mcsr |= S5933_MCSR_RTE;

  /* Initialises the INTCSR register */
  (*device)->intcsr = 0x00;

  /* Enables or disables the byte swapping as requested */
  if ((*device)->params.byte_swapping == SLINK_ENABLE) {
    (*device)->intcsr &= ~S5933_INTCSR_EC;
    (*device)->intcsr |=  S5933_INTCSR_32BITEC;
  }
  else {
    (*device)->intcsr &= ~S5933_INTCSR_EC;
  }

  /* Writes on the output register */
  WriteReg(regs, SPS_OUT_REG, (*device)->specific.sps.out);

  /* Writes on the MCSR */
  WriteReg(regs, S5933_MCSR, (*device)->mcsr);

  /* Writes on the INTCSR */
  WriteReg(regs, S5933_INTCSR, (*device)->intcsr);

  /* Initializes the device structure */
  (*device)->struct_id = SPS_STRUCTURE_ID;

  /* Resets the SPS interface */
  if ((r_code = SPS_Reset(*device))) 
  {
    ReturnCode temp = 0;
    int occ = params->occurence;
    
    temp = IO_PCIMemUnmap((u_long)sps_devices[occ].vadd, _1KBYTE);
    if (temp)
      show(("SLINK_SPSOpen: IO_PCIMemUnmap() FAILED [%#x] occ. %d\n", temp, occ));

    temp = IO_PCIDeviceUnlink(params->handle);
    if (temp)
      show(("SLINK_SPSOpen: IO_PCIDeviceUnlink() FAILED [%#x] occ. %d\n", temp, occ));
          
    temp = IO_Close();
    if (temp)
      show(("SLINK_SPSOpen: IO_Close() FAILED [%#x] occ. %d\n", temp, occ));

    /* FIXME: Here should return uio_malloc memory */

    return r_code;
  }

  /* Check if LDOWN# is high */
  if ((ReadReg(regs, SPS_IN_REG) & SPS_LDOWN) == 0) {
    show(("SLINK_SPSOpen: Reseting the LSC\n"));
    
    if ((r_code = LSC_Reset(*device))) 
    {
      ReturnCode temp = 0;
      int occ = params->occurence;

      temp = IO_PCIMemUnmap((u_long)sps_devices[occ].vadd, _1KBYTE);
      if (temp)
        show(("SLINK_SPSOpen: IO_PCIMemUnmap() FAILED [%#x] occ. %d\n", temp, occ));

      temp = IO_PCIDeviceUnlink(params->handle);
      if (temp)
        show(("SLINK_SPSOpen: IO_PCIDeviceUnlink() FAILED [%#x] occ. %d\n", temp, occ));

      temp = IO_Close();
      if (temp)
        show(("SLINK_SPSOpen: IO_Close() FAILED [%#x] occ. %d\n", temp, occ));

      /* FIXME: Here should return uio_malloc memory */

      return r_code;
    }
  }

  /* If we get this far everything ok so ... register this device as in use */
  sps_devices[params->occurence].used = TRUE;

  devices_open++;

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************************/
/* Name           : SLINK_SPSClose                                          */
/* Description    : Removes the access to a SPS interface and frees the     */
/*                  ressources.                                             */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : An error code                                           */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
ReturnCode SLINK_SPSClose(SLINK_device *device)
{
  ReturnCode r_code;
  int occ;

  DEBUG_TEXT(DFDB_SLINK, 15, "SLINK_SPSClose: called");
  OPEN_STATE;

  VALID_DEVICE;

  occ = device->params.occurence;

  VALID_OCCURENCE(occ);

  /* Check that device has been opened */
  if (!sps_devices[occ].used)
    return RCC_ERROR_RETURN(0x00, SLINK_DEVICE_NOT_FOUND);

  /* Checks it's a right pointer */
  if (device == NULL)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);

  /* Checks if the device pointer is right */
  if (SPS_bad_structure(device) == TRUE)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);

  /* Un-register device in use */
  sps_devices[occ].used = FALSE;
  
  r_code = IO_PCIMemUnmap((u_long)sps_devices[occ].vadd, _1KBYTE);
  if (r_code)
    show(("SLINK_SPSClose: IO_PCIMemUnmap() FAILED [%#x] occ. %d\n", r_code, occ));

  r_code = IO_PCIDeviceUnlink(device->params.handle);
  if (r_code)
    show(("SLINK_SPSClose: IO_PCIDeviceUnlink() FAILED [%#x] occ. %d\n", r_code, occ));

  devices_open--;

  /* Frees the memory occupied by the device structure */
  /* FIXME: Here should return uio_malloc memory */


  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************************/
/* Name           : SPS_WriteOneWord                                        */
/* Description    : Writes a ctl or data word on the specified device       */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/* word                | long               | The word to send.             */
/* ------------------------------------------------------------------------ */
/* type_of_word        | int                | To tell if it's a control or  */
/*                     |                    | a data word.                  */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : An error code                                           */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
ReturnCode SPS_WriteOneWord(SLINK_device *device, long word, int type_of_word)
{
  register volatile s5933_regs *regs;

  DEBUG_TEXT(DFDB_SLINK, 15, "SPS_WriteOneWord: called");
  OPEN_STATE;

  VALID_OCCURENCE(device->params.occurence);

#if defined(POINTER_CHECK)
  /* Only for POINTER_CHECK : Checks if the device pointer is right */
  if (SPS_bad_structure(device) == TRUE)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);
#endif

  /* Loads the address of registers */
  regs = device->regs;

#if defined(PARAMETER_CHECK)
  /* Checks the type of word */
  if ((type_of_word < 0) || (type_of_word > 1))
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_PARAMETERS);
#endif

#if defined(LDOWN_CHECK)
  /* Checks LDOWN# */
  if ((ReadReg(regs, SPS_IN_REG) & SPS_LDOWN) == 0)
    return RCC_ERROR_RETURN(0x00, SLINK_DEVICE_DOWN);
#endif

  if (device->transfer_status != SLINK_FREE)
    return RCC_ERROR_RETURN(0x00, SLINK_TRANSFER_NOT_FINISHED);

  /* Checks the type of previuos word sent and changes it if necessary */
  if (type_of_word != (device->specific.sps.out & SPS_UCTRL)) {

    if (ReadReg(regs, S5933_MCSR) & S5933_MCSR_P2AFE) {

      device->specific.sps.out = type_of_word << SPS_UCTRL_BPOS;

      WriteReg(regs, SPS_OUT_REG, device->specific.sps.out);
    }
    else {
      /* Returns if we can't write into the fifo */
      return RCC_ERROR_RETURN(0x00, SLINK_FIFO_FULL);
    }
  }
  else {
    /* Checks that the FIFO is not full */
    /* Returns if we can't write into the fifo */
    if (ReadReg(regs, S5933_MCSR) & S5933_MCSR_P2AFF) {
      return RCC_ERROR_RETURN(0x00, SLINK_FIFO_FULL);
    }
  }

  /* Send the word */
  WriteReg(regs, S5933_FIFO, (unsigned int) word);

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************************/
/* Name           : SPS_InitWrite                                           */
/* Description    : Initializes a device read master DMA operation          */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/* address             | char *             | The PCI physical address      */
/*                     |                    | where the DMA starts.         */
/*                     |                    | Or the virtual address where  */
/*                     |                    | the CPU has to read           */
/* ------------------------------------------------------------------------ */
/* size                | int                | The number of bytes to write  */
/* ------------------------------------------------------------------------ */
/* packet_format       | SLINK_packet_format| To tell if we have to write   */
/*                     |                    | data words between two        */
/*                     |                    | control words or not.         */
/* ------------------------------------------------------------------------ */
/* dma                 | int                | To tell if we have to use the */
/*                     |                    | DMA feature or just the CPU   */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : An error code                                           */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
ReturnCode SPS_InitWrite(SLINK_device *device, char *address, int size,
			 SLINK_packet_format packet_format, int dma)
{
  register volatile s5933_regs *regs;

  DEBUG_TEXT(DFDB_SLINK, 15, "SPS_InitWrite: called");
  OPEN_STATE;

  VALID_OCCURENCE(device->params.occurence);

#if defined(POINTER_CHECK)
  /* Only for POINTER_CHECK : Checks if the device pointer is right */
  if (SPS_bad_structure(device) == TRUE)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);
#endif

#if defined(PARAMETER_CHECK)
  if (packet_format > SLINK_BOTH_CONTROL_WORDS)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_PARAMETERS);
#endif

  /* Loads the address of registers */
  regs = device->regs;

#if defined(LDOWN_CHECK)
  /* Checks LDOWN# */
  if ((ReadReg(regs, SPS_IN_REG) & SPS_LDOWN) == 0)
    return RCC_ERROR_RETURN(0x00, SLINK_DEVICE_DOWN);
#endif

  /* Analyses current transfer status */
  switch (device->transfer_status) {
  case SLINK_FREE:
    device->error_messages  = SLINK_NO_ERROR;
    device->transfer_status = SLINK_SENDING;
    break;

  default:
    return RCC_ERROR_RETURN(0x00, SLINK_TRANSFER_NOT_FINISHED);
    break;
  }

  /* Intialises the transfer parameters in the device structure */
  device->dma                        = dma;
  device->initial_address            = address;
  device->initial_size               = size;
  device->specific.sps.packet_format = packet_format;

  /* If a control word at beginning of packets has to be sent */
  if ((packet_format & SLINK_MASK_START_WORD) != 0) {
    device->specific.sps.where_in_packet       = 1;
    device->specific.sps.phase                 = 0;
  }
  else {
    /* If the start control word flag isn't set */
    device->specific.sps.where_in_packet = 2;
    device->specific.sps.phase           = 0;
  }

  /* If the CPU is used, initializes additional parameters */
  if (dma == SLINK_DISABLE) {
    device->number_words    = size >> 2;
    device->current_address = (long *) address;
  }

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************************/
/* Name           : SPS_ControlAndStatus                                    */
/* Description    : Gives the current status of the writing operation       */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/* status              | int *              | The current status            */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : An error code                                           */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
ReturnCode SPS_ControlAndStatus(SLINK_device *device, int *status)
{
  register volatile s5933_regs *regs;
  register int dma, packet_format, ask_for, where_in_packet, phase;
  register int timeout, free_room, remaining_words, words_to_write, i;
  register long *address;

  DEBUG_TEXT(DFDB_SLINK, 15, "SPS_ControlAndStatus: called");
  OPEN_STATE;

  VALID_OCCURENCE(device->params.occurence);

#if defined(POINTER_CHECK)
  /* Only for POINTER_CHECK : Checks if the device pointer is right */
  if (SPS_bad_structure(device) == TRUE)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);
#endif

  /* Loads the address of registers */
  regs = device->regs;

#if defined(LDOWN_CHECK)
  if ((ReadReg(regs, SPS_IN_REG) & SPS_LDOWN) == 0)
    return RCC_ERROR_RETURN(0x00, SLINK_DEVICE_DOWN);
#endif

  /* Analyses current transfer status */
  switch (device->transfer_status) {
  case SLINK_SENDING:
    break;

  case SLINK_FREE:
    return RCC_ERROR_RETURN(0x00, SLINK_TRANSFER_NOT_STARTED); 
    break;

  default:
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_STATUS);
    break;
  }

  /* Gets the relevant parameters */
  dma = device->dma;
  packet_format = device->specific.sps.packet_format;
  timeout = device->params.timeout;

  /* phase is a sub phase of where_in_packet */
  phase = device->specific.sps.phase;

  /* where_in_packet says where we are in processing a data packet */
  where_in_packet = device->specific.sps.where_in_packet;

  /* Do a loop on the timeout */
  do {
    timeout--;

    /* If initial phase */
    if (phase == 0) {

      /* Looks what type of words we have to ask by using the variable
	 ask_for */
      switch (where_in_packet) {
      case 1:
	ask_for = SLINK_CONTROL_WORD;
	break;

      case 2:
	ask_for = SLINK_DATA_WORD << SPS_UCTRL_BPOS;
	break;

      case 3:
	ask_for = SLINK_CONTROL_WORD;
	break;

      default:
	/* There is a bug somewhere */
	return RCC_ERROR_RETURN(0x00, SLINK_LOST_IN_FUNCTION);
      }

      /* If the UCTRL# line has to be changed */
      if ((device->specific.sps.out & SPS_UCTRL) != ask_for) {
	/* If the fifo is empty */
	if ((ReadReg(regs, S5933_MCSR) & S5933_MCSR_P2AFE)) {

	  /* Clear then change the UCTRL# line */
	  device->specific.sps.out &= ~SPS_UCTRL;
	  device->specific.sps.out |= ask_for;

	  WriteReg(regs, SPS_OUT_REG, device->specific.sps.out);

	  /* We did the initial phase */
	  phase = 1;
	}
      }
      else {
	phase = 1; /* No need to change UCTRL# */
      }
    }

    if (phase >= 1) {
      /* Look where the packet is being processed */
      switch (where_in_packet) {
      case 1:
	/* If the first control word isn't sent, do it */
	WriteReg(regs, S5933_FIFO, device->params.start_word);

	/* And goes to the next step */
	where_in_packet = 2;
	phase = 0;
	break;

      case 2:
	/* If we are sending data words */
	if (dma) {
	  /* If we use dma, looks where we are */
	  switch (phase) {
	  case 2:
	    /* Looks if the DMA operation is finished */
	    if (ReadReg(regs, S5933_MCSR) & S5933_MCSR_P2ATC) {

	      /* If the transfer is finished, go to the next step */
	      where_in_packet = 3;
	      phase = 0;
	    }
	    else {
	      /* Leave loop if the transfer is not finished */
	      timeout = 0;
	    }
	    break;

	  case 1:
	    /* Initialize DMA */
	    WriteReg(regs, S5933_MRAR, (u_long) device->initial_address);
	    WriteReg(regs, S5933_MRTC, (u_long) device->initial_size);

	    /* and we go to the next step */
	    phase = 2;

	    /* Decides to go out from the loop */
	    timeout = 0;

	    break;

	  default:
	    /* There is a bug somewhere */
	    return RCC_ERROR_RETURN(0x00, SLINK_LOST_IN_FUNCTION);
	    break;
	  }
	}
	else {
	  /* If we use CPU to write words on S-LINK */
	  /* Gets the number of words to write */
	  remaining_words = device->number_words;

	  /* Gets the address where to read words */
	  address = device->current_address;

	  while (timeout-- > 0) {
	    /* Reads the MCSR register */
	    device->mcsr = ReadReg(regs, S5933_MCSR);

	    /* Checks the state of the AMCC fifo */
	    if (device->mcsr & S5933_MCSR_P2AFE) {
	      free_room = 8;
	    }
	    else {
	      if (device->mcsr & S5933_MCSR_P2AF4) {
		free_room = 4;
	      }
	      else {
		if ((device->mcsr & S5933_MCSR_P2AFF) == 0) {
		  free_room = 1;
		}
		else {
		  free_room = 0;
		}
	      }
	    }

	   words_to_write = (free_room > remaining_words) ?
	     remaining_words : free_room;

	   /* Writes the words in the fifo until we lost the right to do it */
	    for (i=0; i<words_to_write; i++) {
	      WriteReg(regs, S5933_FIFO, (unsigned int) *(address++));
	    }

	    /* Calculates the remaining size */
	    remaining_words += -(words_to_write);

	    /* Decrements the timeout with the amount of written words */
	    timeout += -(words_to_write);

	    /* If the transfer is finished, go to the next step */
	    if (remaining_words <= 0) {
	      where_in_packet = 3;

	      /* Go directly to phase 1 if we don't need to send EoB */
	      if ((packet_format & SLINK_MASK_STOP_WORD) != 0)
		phase = 0;
	      else
		phase = 1;
	      
	      break;
	    }
	  } /* End of while timeout */

	  device->number_words = remaining_words;
	  device->current_address = address;
	} /* End of CPU reading */

	break;

      case 3:
	/* Only if we have to send control words */
	if ((packet_format & SLINK_MASK_STOP_WORD) != 0) {
	  WriteReg(regs, S5933_FIFO, device->params.stop_word);
	}

	/* Increments statistics */
	device->stats.good_packets++;

	/* And ends the transfer */
	device->transfer_status = SLINK_FREE;

	/* Informs the user that the transfer is finished */
	*status = SLINK_FINISHED;

	return RCC_ERROR_RETURN(0x00, SLINK_OK);

	break;

      default:
	/* There is a bug somewhere */
	return RCC_ERROR_RETURN(0x00, SLINK_LOST_IN_FUNCTION); 
	break;

      } /* End of switch( where_in_packet) */
    } /* End of if it'sn't the initial phase */
  } while (timeout > 0); /* End of while */

  /* Saves the variables */
  device->specific.sps.phase = phase;
  device->specific.sps.where_in_packet = where_in_packet;

  /* Returns the status */
  *status = device->transfer_status;

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************************/
/* Name           : LDC_Reset                                               */
/* Description    : Resets the LDC                                          */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Return value : An error code                                             */
/*                                                                          */
/* Notes        : 970722 JOP                                                */
/*                Implement according to new SSP user's guide		    */
/*                                                                          */
/****************************************************************************/
ReturnCode LDC_Reset(SLINK_device *device)
{
  static volatile s5933_regs *regs;
  int timeout = 0;

  DEBUG_TEXT(DFDB_SLINK, 15, "LDC_Reset: called");
  OPEN_STATE;

  VALID_OCCURENCE(device->params.occurence);

  /* Checks if the device pointer is right */
  if (SSP_bad_structure(device) == TRUE)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);

  /* Gets the address of the registers */
  regs = device->regs;

  device->specific.ssp.out &= ~SSP_URESET;

  WriteReg(regs, SSP_OUT_REG, device->specific.ssp.out);

  /* Waits few microseconds */
  SLINK_Sleep(10);

  /* Waits until LDOWN# is removed unless the timeout expired */
  do {
    /* Reads the ingoing mailbox */
    device->specific.ssp.in = ReadReg(regs, SSP_IN_REG);

    /* Waits a milli second */
    SLINK_Sleep(1000);

    /* Increases timeout */
    timeout++;

    /* Returns an error if the timeout is expired */
    if (timeout > SLINK_RESET_TIMEOUT)
      return RCC_ERROR_RETURN(0x00, SLINK_DOWN_LINK);

  } while ((device->specific.ssp.in & SSP_LDOWN) == 0);

  device->specific.ssp.out |= SSP_URESET;
  WriteReg(regs, SSP_OUT_REG, device->specific.ssp.out);
  
  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************************/
/* Name           : SSP_Reset                                               */
/* Description    : Resets a SSP interface                                  */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : An error code                                           */
/*                                                                          */
/* Notes          : 970722 JOP                                              */
/*                  Change reset sequence a la SSP user's guide             */
/*                  Call LDC_Reset to reset the link	         	    */
/*                                                                          */
/*                  970820 Michaela                                         */
/*                  SSP_Reset does the reset of the SSP interface and not   */
/*                  the reset of the SLINK                                  */
/****************************************************************************/
ReturnCode SSP_Reset(SLINK_device *device)
{
  static volatile s5933_regs *regs;

  DEBUG_TEXT(DFDB_SLINK, 15, "SSP_Reset: called");
  OPEN_STATE;

  VALID_OCCURENCE(device->params.occurence);

  /* Checks if the device pointer is right */
  if (SSP_bad_structure(device) == TRUE)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);

  /* Gets the address of the registers */
  regs = device->regs;

  /* Resets the add-on fifo */
  device->specific.ssp.out &=  ~SSP_FRESET;
  WriteReg(regs, SSP_OUT_REG, device->specific.ssp.out);

  /* Waits few microseconds */
  SLINK_Sleep(10);

  /* Resets the add-on card */
  WriteReg(regs, S5933_MCSR, (ReadReg(regs, S5933_MCSR) | S5933_MCSR_AOR));

  SLINK_Sleep(10);

  /* Resets the S593X's fifo */
  WriteReg(regs, S5933_MCSR, (ReadReg(regs, S5933_MCSR) | S5933_MCSR_A2PFR));

  SLINK_Sleep(10);

  /* Removes the reset signal on the add-on card */
  WriteReg(regs, S5933_MCSR, (ReadReg(regs, S5933_MCSR) & ~S5933_MCSR_AOR));

  SLINK_Sleep(10);

  /* Removes the reset of the add-on fifo */
  device->specific.ssp.out |= SSP_FRESET;
  WriteReg(regs, SSP_OUT_REG, device->specific.ssp.out);

  device->transfer_status = SLINK_FREE;
  
  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************************/
/* Name           : SLINK_SSPOpen                                           */
/* Description    : Gives access to a SSP interface and initializes it.     */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* params              | SLINK_parameters*  | Includes the initializing     */
/*                     |                    | parameters.                   */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device **    | Contains the address of a     */
/*                     |                    | structure defining the chip   */
/*                     |                    | when the function returned    */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : An error code                                           */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
ReturnCode SLINK_SSPOpen(SLINK_parameters *params, SLINK_device **device)
{
  ReturnCode r_code;
  unsigned long virt;
  static volatile s5933_regs *regs;

  DEBUG_TEXT(DFDB_SLINK, 15, "SLINK_SSPOpen: called");
  OPEN_STATE;

  MAX_DEVICES_OPEN;

  VALID_OCCURENCE(params->occurence);

  *device = (SLINK_device *) -1;
    
  /* Only carry on if this device not already allocated */
  if (ssp_devices[params->occurence].used)
    return RCC_ERROR_RETURN(0, SLINK_DEVICEOPEN);

  r_code = PCI_Stuff(SSP_VENDOR, SSP_DEVICE, params->occurence, &virt, &params->handle);
  if (r_code)
    return r_code;
    
  ssp_devices[params->occurence].vadd = (char *)virt;

  regs = (s5933_regs *)virt;

  /* Allocates memory for the device structure */
  if ((*device = (SLINK_device *) uio_malloc(sizeof(SLINK_device))) == NULL){
    ReturnCode temp;
    int occ = params->occurence;

    temp = IO_PCIMemUnmap((u_long)ssp_devices[occ].vadd, _1KBYTE);
    if (temp)
      show(("SLINK_SSPOpen: IO_PCIMemUnmap() FAILED [%#x] occ. %d\n", temp, occ));

    temp = IO_PCIDeviceUnlink(params->handle);
    if (temp)
      show(("SLINK_SSPOpen: IO_PCIDeviceUnlink() FAILED [%#x] occ. %d\n", temp, occ));

    temp = IO_Close();
    if (temp)
      show(("SLINK_SSPOpen: IO_Close() FAILED [%#x] occ. %d\n", temp, occ));

    return RCC_ERROR_RETURN(0x00, SLINK_NOT_ENOUGH_MEMORY);
  }

  bcopy((char *) params, (char *) &((*device)->params), sizeof(SLINK_parameters));

  bzero((char *) &((*device)->stats), sizeof(SLINK_statistics));

  (*device)->regs = regs;

  (*device)->transfer_status = SLINK_FREE;

  (*device)->error_messages = SLINK_NO_ERROR;

  (*device)->initial_size = 0x00;

  (*device)->packet_size = 0x00;

  (*device)->dma = SLINK_ENABLE;

  /* Rejects overflown buffers by default */
  (*device)->specific.ssp.multiple_pages_transfer = SLINK_DISABLE;

  /* Initialises the output S-LINK register */
  (*device)->specific.ssp.out = 0x00;

  /* Clear and set the data width */
  (*device)->specific.ssp.out &= ~SSP_UDWS;
  (*device)->specific.ssp.out |= ((*device)->params.data_width & 0x3) << SSP_UDWS_BPOS;

  /* Expects control words */
  (*device)->specific.ssp.out |= SLINK_CONTROL_WORD;

  /* Expects no errors */
  (*device)->specific.ssp.out |= SSP_EXPLDERR;

  /* Sets the URESET#, UTDO#, FRESET# and URLs signals */
  (*device)->specific.ssp.out |=  SSP_URESET;
  (*device)->specific.ssp.out |=  SSP_UTDO;
  (*device)->specific.ssp.out |=  SSP_FRESET;
  (*device)->specific.ssp.out &= ~SSP_URLS;

  /* Initialises the MCSR register */
  (*device)->mcsr = 0x00;

#if defined(REFRAINED_REQUEST)
  /* DMA asks for bus mastering only if 4 or more words in S5933 fifo */
  (*device)->mcsr |= S5933_MCSR_WMS;
#endif

  /* Resets the DMA counter */
  WriteReg(regs, S5933_MWTC, 0x00);

  /* Initialises the INTCSR register */
  (*device)->intcsr = 0x0;

  /* Enables or disables the byte swapping as requested */
  if ((*device)->params.byte_swapping == SLINK_ENABLE) {
    (*device)->intcsr &= ~S5933_INTCSR_EC;
    (*device)->intcsr |= S5933_INTCSR_32BITEC;
  }
  else {
    (*device)->intcsr &= ~S5933_INTCSR_EC;
  }

  /* Writes on the out-going mailbox */
  WriteReg(regs, SSP_OUT_REG, (*device)->specific.ssp.out);

  /* Writes on the MCSr */
  WriteReg(regs, S5933_MCSR, (*device)->mcsr);

  /* Writes on the INTCSR */
  WriteReg(regs, S5933_INTCSR, (*device)->intcsr);

  /* Initializes the device structure */
  (*device)->struct_id = SSP_STRUCTURE_ID;

  /* Resets the SSP interface */
  if ((r_code = SSP_Reset(*device))) {
    ReturnCode temp;
    int occ = params->occurence;

    temp = IO_PCIMemUnmap((u_long)ssp_devices[occ].vadd, _1KBYTE);
    if (temp)
      show(("SLINK_SSPOpen: IO_PCIMemUnmap() FAILED [%#x] occ. %d\n", temp, occ));

    temp = IO_PCIDeviceUnlink(params->handle);
    if (temp)
      show(("SLINK_SSPOpen: IO_PCIDeviceUnlink() FAILED [%#x] occ. %d\n", temp, occ));

    temp = IO_Close();
    if (temp)
      show(("SLINK_SSPOpen: IO_Close() FAILED [%#x] occ. %d\n", temp, occ));

    /* FIXME: Here should return uio_malloc memory */

    return r_code;
  }

  /* Checks if SLINK is LDOWN, and if so reset SLINK */
  if ((ReadReg(regs, SSP_IN_REG) & SSP_LDOWN) == 0) {
    show(("Reseting the SLINK\n"));

    if ((r_code = LDC_Reset(*device))) {
      ReturnCode temp;
      int occ = params->occurence;
      
      temp = IO_PCIMemUnmap((u_long)ssp_devices[occ].vadd, _1KBYTE);
      if (temp)
        show(("SLINK_SSPOpen: IO_PCIMemUnmap() FAILED [%#x] occ. %d\n", temp, occ));

      temp = IO_PCIDeviceUnlink(params->handle);
      if (temp)
        show(("SLINK_SSPOpen: IO_PCIDeviceUnlink() FAILED [%#x] occ. %d\n", temp, occ));

      temp = IO_Close();
      if (temp)
        show(("SLINK_SSPOpen: IO_Close() FAILED [%#x] occ. %d\n", temp, occ));

      /* FIXME: Here should return uio_malloc memory */

      return r_code;
    }
  }

  /* Saves the value of the in-going mailbox */
  (*device)->specific.ssp.previous_in = ReadReg(regs, SSP_IN_REG);

  /* Sets the expecting values for the transfer lines LCTRL# and LDERR# */
  /* to the values in the in register */
  (*device)->specific.ssp.out &= ~SSP_EXPLINES;
  (*device)->specific.ssp.out |= ((*device)->specific.ssp.previous_in & SSP_LINES);

  WriteReg(regs, SSP_OUT_REG, (*device)->specific.ssp.out);

  /* Enables the DMA if we expect data words */
  if ((*device)->specific.ssp.previous_in & SSP_LCTRL) {
    (*device)->mcsr |= S5933_MCSR_WTE;
  }
  else {
    (*device)->mcsr &= ~S5933_MCSR_WTE;
  }

  /* Writes on the MCSR */
  WriteReg(regs, S5933_MCSR, (*device)->mcsr);

  /* If we get this far everything ok so ... register this device as in use */
  ssp_devices[params->occurence].used = TRUE;

  devices_open++;

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************************/
/* Name           : SLINK_SSPClose                                          */
/* Description    : Removes the access to a SSP interface and frees the     */
/*                  ressources.                                             */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : An error code                                           */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
ReturnCode SLINK_SSPClose(SLINK_device *device)
{
  ReturnCode r_code;
  int occ;

  DEBUG_TEXT(DFDB_SLINK, 15, "SLINK_SSPClose: called");
  OPEN_STATE;

  VALID_DEVICE;

  occ = device->params.occurence;

  VALID_OCCURENCE(occ);

  /* Check that device has been opened */
  if (!ssp_devices[occ].used)
    return RCC_ERROR_RETURN(0x00, SLINK_DEVICE_NOT_FOUND);

  /* Checks it's a right pointer */
  if (device == NULL)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);

  /* Checks if the device pointer is right */
  if (SSP_bad_structure(device) == TRUE)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);

  /* Un-register device in use */
  ssp_devices[occ].used = FALSE;

  r_code = IO_PCIMemUnmap((u_long)ssp_devices[occ].vadd, _1KBYTE);
  if (r_code)
    show(("SLINK_SSPClose: IO_PCIMemUnmap() FAILED [%#x] occ. %d\n", r_code, occ));

  r_code = IO_PCIDeviceUnlink(device->params.handle);
  if (r_code)
    show(("SLINK_SSPClose: IO_PCIDeviceUnlink() FAILED [%#x] occ. %d\n", r_code, occ));

  devices_open--;

  /* Frees the memory occupied by the device structure */
  /* FIXME: Here should return uio_malloc memory */

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************************/
/* Name           : SSP_ReadOneWord                                         */
/* Description    : Reads one control or data word on the specified SLINK   */
/*                  device.                                                 */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/* word                | long *             | A variable to store the read  */
/*                     |                    | word.                         */
/* ------------------------------------------------------------------------ */
/* type_of_word        | int *              | To tell if it's a control or  */
/*                     |                    | a data word.                  */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : A error message                                         */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
ReturnCode SSP_ReadOneWord(SLINK_device *device, long *word, int *type_of_word)
{
  register volatile s5933_regs *regs;

#if defined(FIFO_WAIT)
  volatile int t;
#endif
  DEBUG_TEXT(DFDB_SLINK, 15, "SSP_ReadOneWord: called");

#if defined(POINTER_CHECK)
  /* Checks if the device pointer is right */
  if (SSP_bad_structure( device) == TRUE)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);
#endif

  /* Loads the address of registers */
  regs = device->regs;

  /* Reads the in-going register */
  device->specific.ssp.in = ReadReg(regs, SSP_IN_REG);

#if defined(LDOWN_CHECK)
  /* Checks LDOWN# */
  if ((device->specific.ssp.in & SSP_LDOWN) == 0)
    return RCC_ERROR_RETURN(0x00, SLINK_DEVICE_DOWN);
#endif

  if (device->transfer_status != SLINK_FREE)
    return RCC_ERROR_RETURN(0x00, SLINK_TRANSFER_NOT_FINISHED);

  /* Checks that the FIFO is not empty */
  if (ReadReg(regs, S5933_MCSR) & S5933_MCSR_A2PFE) {

    /* Compares the values of LCTRL# and LDERR# to their previous values */
    if ((device->specific.ssp.previous_in & SSP_LINES) == \
	(device->specific.ssp.in & SSP_LINES)) {

      /* No change so the fifo is really empty */
      return RCC_ERROR_RETURN(0x00, SLINK_FIFO_EMPTY);
    }
    else {
      /* Checks the fifo a second time */
      if (ReadReg(regs, S5933_MCSR) & S5933_MCSR_A2PFE) {

	/* If the fifo is still empty, change the expected transfer lines */
	device->specific.ssp.out &=  ~SSP_EXPLINES;
	device->specific.ssp.out |= (device->specific.ssp.in & SSP_LINES);

	WriteReg(regs, SSP_OUT_REG, device->specific.ssp.out);

	/* Saves the new ingoing-mailbox in the old copy */
	device->specific.ssp.previous_in = device->specific.ssp.in;

	/* Now there are two different philosophy : If FIFO_WAIT is
	   chosen, we have to care about PCI retry ( if we don't want
	   to crash the board, so we wait the fifo is no more empty
	   before we read it.If FIFO_CHECK is chosen without FIFO_WAIT, then
	   we signal an error if the fifo is empty */

#if defined(FIFO_WAIT)
	/* If no PCI bus retry, wait until the fifo is not empty */
        while (ReadReg(regs, S5933_MCSR) & S5933_MCSR_A2PFE)
	  SLINK_WaitOneMicro(t);
#else
#if defined(FIFO_CHECK)
	/* Checks that the FIFO is not empty as expected.
	   If the hardware works correctly, this test isn't needed.
	   It's why this test is only done with the FIFO_CHECK option. */
	if (ReadReg(regs, S5933_MCSR) & S5933_MCSR_A2PFE)
	  return RCC_ERROR_RETURN(0x00, SLINK_FIFO_EMPTY);
#endif
#endif
      }
    }
  }

  /* Reads the word */
  *word = ReadReg(regs, S5933_FIFO);

  /* Gets the type of word */
  *type_of_word = device->specific.ssp.previous_in & SSP_LCTRL;

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************************/
/* Name           : SSP_InitRead                                            */
/* Description    : Initializes a device write master DMA operation         */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/* address             | char *             | The PCI physical address      */
/*                     |                    | where the DMA starts.         */
/*                     |                    | Or the virtual address where  */
/*                     |                    | the CPU has to read           */
/* ------------------------------------------------------------------------ */
/* size                | int                | The number of bytes to read   */
/* ------------------------------------------------------------------------ */
/* dma                 | int                | To tell if we have to use the */
/*                     |                    | DMA feature or just the CPU   */
/* ------------------------------------------------------------------------ */
/* multiple_pages      | int                | Allows a one packet transfer  */
/*                     |                    | on several pages              */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : A error message                                         */
/*                                                                          */
/* Notes          :                                                         */
/* Modified       : Jun-1997 by M.Niculescu                                 */
/*                  Here prepare only the device structure and don't write  */
/*                  into  dma registers of S5933                            */
/*                : Nov-1997 by M.Niculescu                                 */
/*                  Allows the state SLINK_NEED_OF_NEW_PAGE                 */
/****************************************************************************/
ReturnCode SSP_InitRead(SLINK_device *device,
			char *address,
			int size,
			int dma,
			int multiple_pages)
{
  register volatile s5933_regs *regs;

  DEBUG_TEXT(DFDB_SLINK, 15, "SSP_InitRead: called");
  OPEN_STATE;

  VALID_OCCURENCE(device->params.occurence);

#if defined(POINTER_CHECK)
  /* Check if the device pointer is right */
  if (SSP_bad_structure(device) == TRUE)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);
#endif

  /* Loads the address of registers */
  regs = device->regs;

#if defined(LDOWN_CHECK)
  /* Checks LDOWN# */
  if ((ReadReg(regs, SSP_IN_REG) & SSP_LDOWN) == 0)
    return RCC_ERROR_RETURN(0x00, SLINK_DEVICE_DOWN);
#endif

  /* Analyses current transfer status */
  switch (device->transfer_status) {
  case SLINK_FREE:
    device->dma = dma;
    device->specific.ssp.multiple_pages_transfer = multiple_pages;

    /* If the device was free, it waits for a packet */
    device->transfer_status = SLINK_WAITING_FOR_SYNC;
    device->error_messages = SLINK_NO_ERROR;
    break;

  case SLINK_NEED_OF_NEW_PAGE:    /* New page to continue dma       */
                                  /* allow dma init on the new page */   
     break;

  case SLINK_WAITING_FOR_SYNC:    /* Ongong transfer, don't allow dma init */
  case SLINK_RECEIVING:
    return RCC_ERROR_RETURN(0x00, SLINK_TRANSFER_NOT_FINISHED);
    break;

  default:
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_STATUS);
    break;
  }

  /* Sets the initial size and inital address */
  device->initial_address = address;
  device->initial_size = size;

  /* If a CPU is asked  */
  if (device->dma != SLINK_ENABLE) {
    device->number_words = size >> 2;
    device->current_address = (long *) address;
  } 

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************************/
/* Name           : SSP_ControlAndStatus                                    */
/* Description    : Gives the current status of the reading operation       */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* device              | SLINK_device *     | Pointer to the chip structure */
/* ------------------------------------------------------------------------ */
/* status              | int *              | The current status            */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : An  error message                                       */
/*                                                                          */
/* Notes          :                                                         */
/* Modified       : Jun -1997 by M. Niculescu                               */
/*                  When checking add on fifo read directly the             */
/*                  S5933 registers, don't use the flag fifo_empty          */ 
/*                                                                          */
/*                  Whenever cpu reads the add on fifo check before that    */
/*                  fifo is realy not empty                                 */
/*                                                                          */
/*                  When reading the packets by using CPU, do the reading   */
/*                  loop until number_words > 0 and stop_flag = 0           */
/*                                                                          */
/* Modified       : Nov. -1997 by M. Niculescu                              */
/*                  Ask for a new page to continue dma transfer             */
/*                  Initialise dma to continue the transfer on the new page */
/****************************************************************************/
ReturnCode SSP_ControlAndStatus(SLINK_device *device, int *status)
{
  register volatile s5933_regs *regs;
  static int words_in_fifo;
  static int control_line, error_line;
  static int old_control_line;
  static int dma, timeout;
  static unsigned int control_word;
  static int init_page = 0;
  static int number_words, words_to_read;
  static long * address;
  static int stop_flag, i;
  int word;

  DEBUG_TEXT(DFDB_SLINK, 15, "SSP_ControlAndStatus: called");
  OPEN_STATE;

  VALID_OCCURENCE(device->params.occurence);

#if defined(POINTER_CHECK)
  /* Checks if the device pointer is right */
  if (SSP_bad_structure(device) == TRUE)
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_POINTER);
#endif

  /* Loads the address of registers */
  regs = device->regs;

#if defined(LDOWN_CHECK)
  /* Checks LDOWN# */
  if ((ReadReg(regs, SSP_IN_REG) & SSP_LDOWN) == 0)
     return RCC_ERROR_RETURN(0x00, SLINK_DEVICE_DOWN);
#endif

  /* Analyses current transfer status */
  switch (device->transfer_status) {
  case SLINK_RECEIVING:
  case SLINK_NEED_OF_NEW_PAGE:     
  case SLINK_WAITING_FOR_SYNC:
    /* Normal modes : continue */
    break;

  case SLINK_FREE:
    return RCC_ERROR_RETURN(0x00, SLINK_TRANSFER_NOT_STARTED);
    break;

  default :
    return RCC_ERROR_RETURN(0x00, SLINK_BAD_STATUS);
    break;
  }

  /* Gets the DMA flag */
  dma = device->dma;

  /* Gets the timeout */
  timeout = device->params.timeout;

  /* Initializes the type of word flag */
  control_line = device->specific.ssp.previous_in & SSP_LCTRL;

  /* Loop on the timeout */
  do {
    /*--------------------------------------------------------------------*/
    /* Part I : Commutation of the expected lines EXPLCTRL# and EXPLDERR# */
    /*--------------------------------------------------------------------*/

    /* Checks that the FIFO is not empty to continue */
    if ((ReadReg(regs, S5933_MCSR) & S5933_MCSR_A2PFE) == 0) {
      DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: part I/1, fifo not empty");
    }
    else {
      /* Reads the in-going register */
      device->specific.ssp.in = ReadReg(regs, SSP_IN_REG);

      /* Compares the values of LCTRL# and LDERR# to their previous values */
      if ((device->specific.ssp.previous_in & SSP_LINES) == \
	  (device->specific.ssp.in & SSP_LINES)) {

	/* If they didn't change  the fifo is really empty */
      }
      else {
	/* expected lines != actual lines, but fifo still not empty */
	/* Checks the fifo a second time */
	if ((ReadReg(regs, S5933_MCSR) & S5933_MCSR_A2PFE) == 0) {
	  /* The fifo is no more empty */
	  DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: part I/2, fifo not empty");
	}

	/* If fifo is still empty, change the expected transfer lines */

	else {
	  /* fifo realy empty, change the expected lines to go on */
	  /* Saves the value of control_line */
	  old_control_line = control_line;

	  /* Gets the flags for the transfer lines LCTRL# and LDERR# */
	  control_line = device->specific.ssp.in & SSP_LCTRL;
	  error_line   = device->specific.ssp.in & SSP_LDERR;

	  /* Signals an error in the current packet if it's the case */
	  if ((error_line & SSP_LDERR) == SLINK_WRONG_WORD) {
	    device->specific.ssp.current_error = 1;
	  }

	  /* Saves the new ingoing-mailbox in the old copy */
	  device->specific.ssp.previous_in = device->specific.ssp.in;

	  /* If the control flag has changed */
	  if (dma == SLINK_ENABLE) {
	    if (control_line != old_control_line) {
	      DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part I/3: control_line = 0x" << std::hex << control_line <<
	               " old_control_line = 0x" << old_control_line << std::dec);
	      /* Disable DMA if we got control words */
	      if (control_line == SLINK_CONTROL_WORD) {
		/* fifo is empty and the next words will be
		   control words read by CPU, not DMA */
		device->mcsr &= ~S5933_MCSR_WTE;

                /* Writes to the MCSR register */
                WriteReg(regs, S5933_MCSR, device->mcsr);
	        DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part I/3: Disable DMA");
	      } 
	    } 
	  } 

	  /* Modifies the expected transfer lines in the out-going mailbox */
	  device->specific.ssp.out &= ~SSP_EXPLINES;
	  device->specific.ssp.out |= device->specific.ssp.in & SSP_LINES;

	  WriteReg(regs, SSP_OUT_REG, device->specific.ssp.out);
	}
      }
    }

    /*-----------------------------------*/
    /* Part II : Synchronisation control */
    /*-----------------------------------*/

    /* Are we receiving control words ? */
    if (control_line == SLINK_CONTROL_WORD) {

      if ((ReadReg(regs, S5933_MCSR) & S5933_MCSR_A2PFE) == 0) {

	/* Read the control word in the fifo */
	control_word = ReadReg(regs, S5933_FIFO);

/******* PCI debugging **********************************
        *(p_sram+10) = BSWAP(control_word);
        *(p_sram+11) = BSWAP(device->params.start_word);
        if( (control_word & (unsigned int)~0xf) != device->params.start_word &&
            (control_word & (unsigned int)~0xf) != device->params.stop_word)
          *p_sram=0x11222211;
*********************************************************/

	DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part II: Receive control word 0x" << std::hex << control_word << std::dec);

	/* We can analyse the control word                  */
	/* If the word is a starting word for a data packet */
#if defined(LINK_4BITS)
	if ((control_word & 0xf0) == (device->params.start_word & 0xF0)) {
#else
	if ((control_word & (unsigned int) ~0xf) == device->params.start_word){
#endif
	  /* If a previous packet was still being received */
	  /* ... then put the error in statistics          */
	  if (device->transfer_status == SLINK_RECEIVING) {
	    device->stats.lost_ends++;
	  }

	  /* Transferring a new packet */
	  device->transfer_status = SLINK_RECEIVING;

	  /* Reset the packet size */
	  device->packet_size = 0;

	  /* Assume there is no error in the current packet */
	  device->specific.ssp.current_error = 0;

	  /* Restart the transfer at the beginning of the page */
	  init_page = 1;

/******* PCI debugging **********************************
          *(p_sram+20) = init_page;
*********************************************************/

	  DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part II: Beginning of a new packet");
	}

	/* If the control word signals the end of a data packet */
#if defined(LINK_4BITS)
	else if ((control_word & 0xf0) == (device->params.stop_word & 0xf0)) {
#else
        else if ((control_word & (unsigned int) ~0xf) == device->params.stop_word) {
#endif

	  /* If we were transferring a data packet before */
	  if (device->transfer_status == SLINK_RECEIVING) {

	    /* If a transmission error occured in this data packet */
	    if (device->specific.ssp.current_error != 0) {

	      /* Inserts it in statistics */
	      device->stats.transmission_errors++;

	      /* If corrupt packets have to be rejected */
	      if (device->params.keep_bad_packets == SLINK_DISABLE) {
		/* Aborts the transfer of the packet and waits for a new one */
		device->transfer_status = SLINK_WAITING_FOR_SYNC;
	      }
	    }

	    /* If we have kept the packet */
	    if (device->transfer_status == SLINK_RECEIVING) {
	      /* Stops the transfer */
	      device->transfer_status = SLINK_FREE;

	      /* Computes the size of the packet */
	      /* If the dma was chosen */
	      if (dma) {
		/* Reads the DMA counter */
		device->packet_size += \
		  device->initial_size - ReadReg(regs, S5933_MWTC);

	        DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part II: End of packet");
	      }
	      /* If the CPU was used */
	      else {
		/* Computes the size */
		device->packet_size += device->initial_size - \
		  (device->number_words << 2);
	      }

	      /* Increments statistics */
	      device->stats.good_packets++;

	      /* Exit from the function and informs the transfer is finished */
	      *status = SLINK_FINISHED;

	      return RCC_ERROR_RETURN(0x00, SLINK_OK);
	    } /* End of test about transmission errors */
	  }
	  else {
	   /* If no transfer was begun, we lost the beginning  of a packet */
           /* Continue the transfer until a SoB control word appears       */
           /* First transfer, be overwritten by the following good packet  */ 
	    device->stats.lost_starts++;
	    DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part II: rec. packet without SoB, device status = " << device->transfer_status);
	  } /* End of test about transfer status */
	}

	/* If we receive another control word */
	else {
	  /* Signals it in the statistics AND return error*/
	  device->stats.unknown_control_words++;
	  DEBUG_TEXT(DFDB_SLINK, 5 ,"SSP_ControlAndStatus: Wrong control word = 0x" << std::hex << control_word << std::dec);
          return RCC_ERROR_RETURN(0x00, SLINK_UNEXPECTED_CW);
	}
      }
    }

    /*--------------------*/
    /* Part III : Reading */
    /*--------------------*/

    /*---------------------------------------------------*/
    /* The reading algorithm depends on using DMA or not */
    /*---------------------------------------------------*/
    if (dma) {
      /* If we are reading data words */
      if (control_line == (SLINK_DATA_WORD << SSP_LCTRL_BPOS)) {
	DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: Reading data words under DMA");

/******* PCI debugging **********************************
        *(p_sram+1) = init_page;
********************************************************/

	/* Reads the MCSR register */
	device->mcsr = ReadReg(regs, S5933_MCSR);

	/* Checks the DMA counter equal 0 */
	if (device->mcsr & S5933_MCSR_A2PTC) {

	  /* If the fifo isn't empty */
	  if ((device->mcsr & S5933_MCSR_A2PFE) == 0) {
	    DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: dma is finished, but there are still data into fifo");
	    DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: device status =" << device->transfer_status);
                      
	    /* allow a dma dummy data transfer to empty the fifo
	       helps to start the syncronization                 */
            if (device->transfer_status == SLINK_WAITING_FOR_SYNC)
               init_page = 1;

/******* PCI debugging **********************************
        *(p_sram+2) = init_page;
*********************************************************/

	    if (device->transfer_status == SLINK_RECEIVING) {
	      /* Ask for a new page to continue the dma transfer */
	      /* Need of a new page to continue dma transfer of  */
	      /* the current packet */
	      device->transfer_status = SLINK_NEED_OF_NEW_PAGE;
	      *status = device->transfer_status;

	      /* update the size of the current  received data */
	      device->packet_size += device->initial_size;
	      DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: dma finished in the current page, read " << device->packet_size << " bytes");
	      return RCC_ERROR_RETURN(0x00, SLINK_OK);
	    } /* End of if status is transfer */

            if (device->transfer_status == SLINK_NEED_OF_NEW_PAGE) {
	      /* Start of a new page in the transfer of the current packet */
              init_page = 1;
              device->transfer_status = SLINK_RECEIVING;
	    } 

	  }
	}
      }

      /* If we have to initialize transfer on the beginning of the page */

/******* PCI debugging **********************************
      *(p_sram+4) = init_page;
*********************************************************/

      if (init_page) {
	/* Resets the flag  */
	init_page = 0;

	/*  Initializes & enable  DMA  */
	WriteReg(regs, S5933_MWAR, (u_long) device->initial_address);
	WriteReg(regs, S5933_MWTC, (unsigned int) device->initial_size);
        WriteReg(regs, S5933_MCSR, (ReadReg(regs, S5933_MCSR) | S5933_MCSR_WTE));

        DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: At beginning of the packet init. & enable DMA");

      } 
    }

    /*---------------------------------*/
    /* If the CPU directly reads words */
    /*---------------------------------*/
    else {
      /* If we are reading data words */
      if (control_line == (SLINK_DATA_WORD << SSP_LCTRL_BPOS)) {
	/* Gets the current transfer parameters */
	address = device->current_address;
	number_words = device->number_words;

        DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: CPU is writing " << number_words << " data words to adress 0x" << std::hex
	<< (uintptr_t) address << std::dec);

	/* We don't want immediately to go out from the loop */
	stop_flag = 0;

	/* do while number_words >0 and stop_flag = 0 */
	do {
	  /* If the init_page flag is not set */
	  if (init_page == 0) {
	    /* If the page is full */
	    if (number_words == 0) {

	      /* If the fifo isn't empty */
	      if ((ReadReg(regs, S5933_MCSR) & S5933_MCSR_A2PFE) == 0) {

		/* If we were receiving a data packet */
		if (device->transfer_status == SLINK_RECEIVING) {
		  /* If we allow a one packet transfer on several pages */
		  if (device->specific.ssp.multiple_pages_transfer ==
		      SLINK_ENABLE) {

		    /* Computes the new packet size */
		    device->packet_size += device->initial_size;

		    /* Asks for a new page */
		    device->transfer_status = SLINK_NEED_OF_NEW_PAGE;

		    /* Stores the event in statistics */
		    device->stats.asks_for_new_page++;

		    /* Exits from the function and returns the status. */
		    *status = device->transfer_status;

		    return RCC_ERROR_RETURN(0x00, SLINK_OK);
		  }
		  else {
		    /* Initalizes the transfer at the beginning of the page */
		    init_page = 1;

		    /* Aborts the transfer */
		    device->transfer_status = SLINK_WAITING_FOR_SYNC;

		    /* Signals it to statistics */
		    device->stats.overflown_buffers++;
		  } /* End of else of if multiple_pages_transfer == enable */
		} /* End of if status = SLINK_RECEIVING */
		else {
		  /* Initalizes the transfer at the beginning of the page */
		  init_page = 1;
		}
	      }
	    }
	  }

	  /* If we have to initialize transfer at the beginning of the page */
	  if (init_page) {
	    /* Resets the flag */
	    init_page = 0;

	    /* Initializes the CPU reading */
	    address = (long *) device->initial_address;
	    number_words = (device->initial_size >> 2);
	    DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: Init. CPU writing " << number_words << " words to address 0x" << std::hex
	    << (uintptr_t) address << std::dec);
	  }

	  /* Reads the MCSR register */
	  device->mcsr = ReadReg(regs, S5933_MCSR);

	  /* Checks the state of the S5933 fifo */
	  if (device->mcsr & S5933_MCSR_A2PFF) {
	    words_in_fifo = 8;
	  }
	  else {
	    if (device->mcsr & S5933_MCSR_A2PF4) {
	      words_in_fifo = 4;
	    }
	    else {
	      if ((device->mcsr & S5933_MCSR_A2PFE) == 0) {
		words_in_fifo = 1;
	      }
	      else {
		words_in_fifo = 0;

		/* The fifo is empty so we have to see what has happened */
		/* so leave the reading loop */
		stop_flag = 1;
	        DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: now stop fifo empty");
	      }
	    }
	  }

	  /* By regarding the number of words in the fifo, and the remaining
	     words to write, reads a given amount of words */
	  words_to_read = (words_in_fifo > number_words) ? number_words : \
	    words_in_fifo;

	  /* Reads the words in the fifo until we lost the right to do it */
	  for (i=0; i<words_to_read; i++) {

            if ((ReadReg(regs, S5933_MCSR) & S5933_MCSR_A2PFE) == 0){

	      word = ReadReg(regs, S5933_FIFO);
	      *address = word; 
	      DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: write to address 0x" << std::hex << (uintptr_t) address << " data 0x"
	      << word << std::dec);
	      address++;
	    }
            else {
	      DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: break because fifo empty");
	      break;
	    }
	  }

	  /* Calculates the remaining size */
	  number_words += -( words_to_read);

	  /* Start again the loop */
	  DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: small loop again, with " << number_words << " words remaining and " << timeout << " as timeout");
	} while((number_words > 0) && ( !stop_flag));

	/* Saves the current transfer parameters */
	device->current_address = address;
	device->number_words = number_words;
        DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: Currently " << number_words << " data words to write to address ox" <<
	std::hex << (uintptr_t) address << std::dec);
      } /* End of if we read data words */
    } /* End of CPU reading */
    DEBUG_TEXT(DFDB_SLINK, 20, "SSP_ControlAndStatus: Part III: big loop again with " << timeout << " as timeout");
  } while (--timeout > 0);
 
  /* Until timeout is exhausted */
  *status = device->transfer_status;

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}

/****************************************************************/
/* This function is equivalent to pushing the RUN/STEP toggle	*/
/* switch on the SLIDAS						*/
/* The transition from 0 to 1 is equivalent to pushing the RUN	*/
/* button - we don't know whether it will START or STOP !!      */
/****************************************************************/
void SLIDAS_PushRun(SLINK_device *dev)
{
  register volatile s5933_regs *regs = dev->regs;

  DEBUG_TEXT(DFDB_SLINK, 15, "SLIDAS_PushRun: called");
  /* set to 0  - keep the other bits */
  dev->specific.ssp.out &= ~SSP_URL0;

  WriteReg(regs, S5933_OMB1, dev->specific.ssp.out);

  usleep(10);

  /* a transition 0-->1 on URL0 */
  dev->specific.ssp.out |= SSP_URL0;

  WriteReg(regs, S5933_OMB1, dev->specific.ssp.out);

  usleep(10);

  /* set to 0  - keep the other bits  */
  dev->specific.ssp.out &= ~SSP_URL0;

  WriteReg(regs, S5933_OMB1, dev->specific.ssp.out);
}

/****************************************************************************/
/* Name           : SLINK_SPSBoot                                           */
/* Description    : Sets some configuration bits that are set to 0 on       */
/*                  machine reboot.                                         */
/*                  DO NOT perform a reset cycle.                           */
/* Parameters     :                                                         */
/* ------------------------------------------------------------------------ */
/* Name                | Type               | Function                      */
/* ------------------------------------------------------------------------ */
/* occurence           | int                | slink ocurrence to set        */
/* ------------------------------------------------------------------------ */
/*                                                                          */
/* Returned value : An error code                                           */
/*                                                                          */
/* Notes          :                                                         */
/*                                                                          */
/****************************************************************************/
ReturnCode SLINK_SPSBoot(int occurence)
{
  ReturnCode r_code;
  unsigned long virt;
  register volatile s5933_regs *regs;
  unsigned int sps_out,sps_in, handle;

  DEBUG_TEXT(DFDB_SLINK, 15, "SLINK_SPSBoot: called");
  OPEN_STATE;

  VALID_OCCURENCE(occurence);

  MAX_DEVICES_OPEN;
    
  /* Only carry on if this device not already allocated */
  if (sps_devices[occurence].used)
    return RCC_ERROR_RETURN(0, SLINK_DEVICEOPEN);
  r_code = PCI_Stuff(SPS_VENDOR, SPS_DEVICE, occurence, &virt, &handle);
  if (r_code)
    return r_code;
    
  sps_devices[occurence].vadd = (char *)virt;

  regs = (s5933_regs *)virt;

  /* Read the SLINK register */
  sps_in = ReadReg(regs, SPS_IN_REG);

  /* Sets the URESET# and UTEST# signals */
  sps_out = sps_in | SPS_URESET;
  sps_out |= SPS_UTEST;

  /* Writes on the output register */
  WriteReg(regs, SPS_OUT_REG, sps_out);

  r_code = IO_PCIMemUnmap(virt, _1KBYTE);
  if (r_code)
    show(("SLINK_SPSBoot: IO_PCIMemUnmap() FAILED [%#x] occ. %d\n", r_code, occurence));

  r_code = IO_PCIDeviceUnlink(handle);
  if (r_code)
    show(("SLINK_SPSBoot: IO_PCIDeviceUnlink() FAILED [%#x] occ. %d\n", r_code, occurence));

  return RCC_ERROR_RETURN(0x00, SLINK_OK);
}


